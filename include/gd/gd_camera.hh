#pragma once

#include <tuple>

#include "gd/gd_types.hh"

/*
 * https://www.youtube.com/watch?v=l9G6MNhfV7M
 */
namespace gd {

class Camera final {
  friend class GDApp;

 public:
  struct View {
    float x{ 0.0 };
    float y{ 0.0 };
    std::int32_t w{ 0 };
    std::int32_t h{ 0 };
  };

  /*
struct Shaking {
    bool is;
    float x{0};
    float y{0};
    double dur;
  };
  */
 public:
  void update ( double delta );
  // void shake ( double screenShake );
  void reset ( );
  void move ( float x, float y, float dur = 0.f );

  float getX ( ) const;
  float getY ( ) const;
  float getXOffset ( ) const;
  float getYOffset ( ) const;
  float getScale ( ) const;
  double getRotation ( ) const;
  std::int32_t getW ( ) const;
  std::int32_t getH ( ) const;

  void setDimension ( std::int32_t w, std::int32_t h );
  void setPosition ( float x, float y );
  void setScale ( float scale );
  void addScale ( float scale );

  void translateX ( float x );
  void translateY ( float y );
  void translate ( float x, float y );

  auto cameraCoords ( ) const {
    //
    return std::make_tuple ( getX ( ), getY ( ) );
  }
  auto cameraCoordsOffset ( ) const {
    //
    return std::make_tuple ( getXOffset ( ), getYOffset ( ) );
  }
  auto toWorldCoords ( float x, float y ) const {
    //
    return std::make_tuple ( view_.x + x, view_.y + y );
  }
  auto toScreenCoords ( float x, float y ) const {
    //
    return std::make_tuple ( -view_.x + x, -view_.y + y );
  }

 private:
  Camera::View view_;
  float zoom_{ 1.0f };
  float uniformScale_{ 1.0f };
  double uniformRotation_{ 0.0 };
};
}  // namespace gd
