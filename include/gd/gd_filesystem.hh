#pragma once
#include <memory>

namespace gd {
class FileSystem final {
  friend class GDApp;

 private:
  void init ( const std::string &basePath ) const;

 public:
  std::tuple< std::unique_ptr< std::uint8_t[] >, std::size_t > load ( const std::string &filePath ) const;
  bool fileExists ( const std::string &file ) const;
  bool dirExists ( const std::string &dir ) const;

  bool mount ( const std::string &archiveOrDir, const char *mountpoint = nullptr ) const;
  bool unmount ( const std::string &archiveOrDir ) const;
};
}  // namespace gd
