#pragma once
#include <optional>

#include "gd/gd_data.hh"
#include "gd/gd_types.hh"
#include "gd/utils/gd_pimpl.hh"

namespace gd {
class Stats final {
  friend class GDApp;

 public:
  struct Time {
    float update;
    float draw;
    double fps;
    double frameTime;
  };

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  Stats ( );
  ~Stats ( );
  Stats ( const Stats & ) = delete;
  Stats &operator= ( const Stats & ) = delete;
  Stats ( Stats && ) = delete;
  Stats &operator= ( const Stats && ) = delete;

 public:
  void update ( double delta );
  void render ( double alpha );
  void reset ( );
  void addTime ( Time &&time );
  void toggle( );
  void show ( );
  void hide ( );
  bool isHidden ( ) const;

 private:
  void init ( );

 public:
  constexpr static auto COUNT{ 200 };
};
}  // namespace gd
