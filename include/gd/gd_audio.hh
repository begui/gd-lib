#pragma once

#include <memory>

#include "gd/utils/gd_pimpl.hh"

namespace gd {

class Audio final {
  friend class GDApp;

 public:
  enum class Type { MUSIC, SFX };

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  Audio ( );
  ~Audio ( );
  Audio ( const Audio & ) = delete;
  Audio &operator= ( const Audio & ) = delete;
  Audio ( Audio && ) = delete;
  Audio &operator= ( const Audio && ) = delete;

 private:
  void init ( );

 public:
  bool load ( Audio::Type audioType, std::int32_t id, const std::string &file );
  void unload ( Audio::Type audioType, std::int32_t id );
  void play ( Audio::Type audioType, std::int32_t id, std::int32_t msFadeIn = 0, bool repeat = false ) const;
  void stop ( Audio::Type audioType, std::int32_t id, std::int32_t msFadeOut = 0 ) const;
  void pause ( Audio::Type audioType, std::int32_t id, bool pause ) const;
  void volume ( Audio::Type audioType, std::int32_t id, std::int32_t vol ) const;
  void volumeUp ( Audio::Type audioType, std::int32_t id, std::int32_t vol = 4 ) const;
  void volumeDown ( Audio::Type audioType, std::int32_t id, std::int32_t vol = -4 ) const;
};
}  // namespace gd
