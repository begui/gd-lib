#pragma once

#include <functional>

#include "gd/gd_data.hh"
#include "gd/gd_types.hh"
#include "gd/utils/gd_pimpl.hh"

namespace gd {

class Window final {
  friend class GDApp;

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  Window ( );
  ~Window ( );
  Window ( const Window & ) = delete;
  Window &operator= ( const Window & ) = delete;
  Window ( Window && ) = delete;
  Window &operator= ( const Window && ) = delete;

 private:
  void init ( );
  void printInfo ( ) const;

 public:
  std::uint32_t windowId ( ) const;
  std::int32_t refreshRate ( ) const;
  std::tuple< int, int > size ( ) const;
  std::tuple< int, int > position ( ) const;
  float opacity ( ) const;

  Window &fullscreen ( bool flag );
  Window &title ( const std::string &title );
  Window &position ( int x, int y );
  Window &opacity ( float op );
  Window &show ( );
  Window &flip ( );
  Window &hide ( );
  Window &raise ( );
  Window &maximize ( );
  Window &minimize ( );
  Window &restore ( );
};

class Texture final {
  friend class GDApp;

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  Texture ( );
  ~Texture ( );
  Texture ( const Texture & ) = delete;
  Texture &operator= ( const Texture & ) = delete;
  Texture ( Texture && ) = delete;
  Texture &operator= ( const Texture && ) = delete;
  bool unload ( cid_t id ) noexcept;
  void color ( cid_t id, const Color &color ) const noexcept;
  ColorOpt_t color ( cid_t id ) const noexcept;
  void blendMode ( cid_t id, const BlendMode mode ) const noexcept;
  BlendMode blendMode ( cid_t id ) const noexcept;
  void alpha ( cid_t id, const std::uint8_t alpha ) const noexcept;
  std::uint8_t alpha ( cid_t id ) const noexcept;
  std::tuple< int, int > size ( cid_t id ) const noexcept;
  // Static Textures
  TextureDataOpt_t create ( const std::string &filename ) const noexcept;
  bool load ( cid_t id, const std::string &filename ) noexcept;
  // Target Textures
  TextureDataOpt_t createTarget ( const std::int32_t w, const std::int32_t h ) const noexcept;
  bool loadTarget ( cid_t id, const std::int32_t w, const std::int32_t h,
                    std::function< void ( ) > &&func = nullptr ) noexcept;
  // Streaming Textures
  TextureDataOpt_t createStream ( const std::int32_t w, const std::int32_t h ) const noexcept;
  bool loadStream ( cid_t id, const std::int32_t w, const std::int32_t h ) noexcept;
  bool updateStream ( TextureData &textureData,                                //
                      std::function< void ( std::uint32_t *pixels ) > &&func,  //
                      const Recti32_t *rect = nullptr ) noexcept;
  bool updateStream ( cid_t id, std::function< void ( std::uint32_t *pixels ) > &&func,  //
                      const Recti32_t *rect = nullptr ) noexcept;
};

#if USE_SDL2_TTF
class FontTTF final {
  friend class GDApp;

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  FontTTF ( );
  ~FontTTF ( );
  FontTTF ( const FontTTF & ) = delete;
  FontTTF &operator= ( const FontTTF & ) = delete;
  FontTTF ( FontTTF && ) = delete;
  FontTTF &operator= ( const FontTTF && ) = delete;

 private:
  void init ( );

 public:
  void setIdAndHeight ( cid_t id, const std::int32_t fontHeight );
  const std::tuple< std::int32_t, std::int32_t > getIdAndHeight ( ) const;

  void setId ( cid_t id );
  void setHeight ( const std::int32_t fontHeight );
  std::int32_t getLineSkip ( ) const;

  bool hasGlyph ( std::uint16_t ch ) const;

 public:
  bool load ( const std::string &file, cid_t id, std::int32_t fontHeight );
  bool loadInternal ( cid_t id, const std::int32_t fontHeight );


 private:
  void writeCachedString ( FontTTFDataOpt_t &fontData,  //
                           const std::string &text,  //
                           const TTFType type,       //
                           const Color &color = Color::WHITE ) const;

 public:
  void writeSolidString ( FontTTFDataOpt_t &fontData,  //
                          const std::string &text,  //
                          const Color &color = Color::WHITE ) const;

  void writeBlendedString ( FontTTFDataOpt_t &fontData,
                            const std::string &text,  //
                            const Color &color = Color::WHITE ) const;

};
#endif

class Renderer final {
  friend class GDApp;

 private:
  class Impl;
  Pimpl< Impl > impl_;

 public:
  Impl &impl ( );
  Impl &impl ( ) const;

 public:
  Renderer ( );
  ~Renderer ( );
  Renderer ( const Renderer & ) = delete;
  Renderer &operator= ( const Renderer & ) = delete;
  Renderer ( Renderer && ) = delete;
  Renderer &operator= ( const Renderer && ) = delete;

 private:
  void init ( const Window &window );
  void printInfo ( ) const;

 public:
  void clear ( ) const;
  void swap ( ) const;

 public:
  void blendMode ( BlendMode mode ) const;
  BlendMode blendMode ( ) const;
  void clearColor ( const Color &color );
  void clip ( Recti32Opt_t srcRect ) const;
  std::tuple< int, int > size ( ) const;
  std::tuple< float, float > sizef ( ) const;
  void size ( const std::int32_t w, const std::int32_t h ) const;
  void sizef ( const float w, const float h ) const;

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  Rectf_t viewportf ( ) const;
#endif
  Recti32_t viewport ( ) const;

 public:
  void drawToTexture ( cid_t id, std::function< void ( ) > &&func ) const;
  void drawToTexture ( const TextureDataOpt_t &texture, std::function< void ( ) > &&func ) const;
  void setTextureTarget ( const TextureDataOpt_t &texture = std::nullopt ) const noexcept;

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  void drawTexture ( cid_t id,                                   //
                     const Recti32Opt_t srcRect = std::nullopt,  //
                     const RectfOpt_t dstRect = std::nullopt,    //
                     const RotationfOpt_t rotation = std::nullopt ) const;
  void drawTexture ( const TextureDataOpt_t &texture,
                     const Recti32Opt_t srcRect = std::nullopt,  //
                     const RectfOpt_t dstRect = std::nullopt,    //
                     const RotationfOpt_t rotation = std::nullopt ) const;
  void drawPoint ( const Pointf_t &p1, const Color &color ) const;
  void drawPoints ( const std::vector< Pointf_t > &pts, const Color &color ) const;
  void drawLine ( const Pointf_t &p1, const Pointf_t &p2, const Color &color ) const;
  void drawLineH ( const float y, const float x1, const float x2, const Color &color ) const;
  void drawLineV ( const float x, const float y1, const float y2, const Color &color ) const;
  void drawLines ( const std::vector< Pointf_t > &pts, const Color &color ) const;
  void drawLines ( const Pointf_t *pts, std::size_t size, const Color &color ) const;
  void drawRect ( const Rectf_t &rect, const Color &color ) const;
  void drawRects ( const std::vector< Rectf_t > &rects, const Color &color ) const;
  void drawRectFill ( const Rectf_t &rect, const Color &color ) const;
  void drawRectsFill ( const std::vector< Rectf_t > &rects, const Color &color ) const;
#else
  void drawTexture ( cid_t id,                                   //
                     const Recti32Opt_t srcRect = std::nullopt,  //
                     const Recti32Opt_t dstRect = std::nullopt,  //
                     const Rotationi32Opt_t rotation = std::nullopt ) const;
  void drawTexture ( const TextureDataOpt_t &texture,            //
                     const Recti32Opt_t srcRect = std::nullopt,  //
                     const Recti32Opt_t dstRect = std::nullopt,  //
                     const Rotationi32Opt_t rotation = std::nullopt ) const;
  void drawPoint ( const Pointi32_t &p1, const Color &color ) const;
  void drawPoints ( const std::vector< Pointi32_t > &pts, const Color &color ) const;
  void drawLine ( const Pointi32_t &p1, const Pointi32_t &p2, const Color &color ) const;
  void drawLineH ( const std::int32_t y, const std::int32_t x1, const std::int32_t x2, const Color &color ) const;
  void drawLineV ( const std::int32_t x, const std::int32_t y1, const std::int32_t y2, const Color &color ) const;
  void drawLines ( const std::vector< Pointi32_t > &pts, const Color &color ) const;
  void drawLines ( const Pointi32_t *pts, std::size_t size, const Color &color ) const;
  void drawRect ( const Recti32_t &rect, const Color &color ) const;
  void drawRects ( const std::vector< Recti32_t > &rects, const Color &color ) const;
  void drawRectFill ( const Recti32_t &rect, const Color &color ) const;
  void drawRectsFill ( const std::vector< Recti32_t > &rects, const Color &color ) const;
#endif
  void drawCircle ( const Circlei32_t &circle, const Color &color ) const;

#if USE_SDL2_TTF
 public:
  void drawTTFFont ( const FontTTFDataOpt_t &text ) const;
#endif
};

}  // namespace gd
