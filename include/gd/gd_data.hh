#pragma once
#include <tuple>
#include <cstdint>

#if USE_SDL2_TTF

namespace gd {
namespace data::font {
constexpr std::int32_t OpenSansRegular{2'000'000'000};
constexpr std::int32_t OpenSansItalic{2'000'000'001};
constexpr std::int32_t OpenSansBold{2'000'000'002};
constexpr std::int32_t OpenSansBoldItalic{2'000'000'003};
constexpr std::int32_t OpenSansExtraBold{2'000'000'004};
constexpr std::int32_t OpenSansExtraBoldItalic{2'000'000'005};
constexpr std::int32_t OpenSansLight{2'000'000'006};
constexpr std::int32_t OpenSansLightItalic{2'000'000'007};
constexpr std::int32_t OpenSansSemiBold{2'000'000'008};
constexpr std::int32_t OpenSansSemiBoldItalic{2'000'000'009};
std::tuple< const unsigned char*, const unsigned int > getFont ( const std::int32_t id );
}  // namespace data::font

namespace data::texture {
constexpr std::int32_t Missing{2'000'000'000};

}  // namespace data::texture
}  // namespace gd

#endif
