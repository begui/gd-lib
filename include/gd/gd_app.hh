#pragma once

#include <optional>
#include <vector>

#include "gd/gd_sdl2.hh"
#include "gd/gd_audio.hh"
#include "gd/gd_camera.hh"
#include "gd/gd_data.hh"
#include "gd/gd_filesystem.hh"
#include "gd/gd_renderer.hh"
#include "gd/gd_stats.hh"
#include "gd/gd_system.hh"
#include "gd/gd_types.hh"

namespace gd {

class Renderer;
class Window;
class Texture;
class FontTTF;
class GDApp {
 public:
  struct Settings {
    struct Window {
      std::int32_t width{ 640 };
      std::int32_t height{ 480 };
      bool fullscreen{ false };
      bool resizable{ false };
      bool maximized{ false };
      bool rendererBatching{ true };
    } window;
    struct Renderer {
      std::int32_t width{ 0 };
      std::int32_t height{ 0 };
      bool integerScale{ true };
      bool renderToTexture{ true };
      bool vsync{ true };
      BlendMode mode{ BlendMode::None };
    } renderer;
    struct Audio {
      std::int32_t frequency{ 0 };
      std::int32_t stereoChannel{ 0 };
      std::int32_t chunksize{ 1024 };
      std::int32_t audioChannel{ 32 };
    } audio;
#if USE_SDL2_TTF
    struct Font {
      struct FontData {
        std::int32_t id;
        std::int32_t height;
        FontData ( const std::int32_t i, const std::int32_t h ) : id{ i }, height{ h } {}
      };
      std::vector< FontData > fonts;
    } font;
#endif
    bool printInfo{ false };
  };

  struct Mouse {
    static void cursor ( bool enable ) noexcept;
    static void relative ( bool enable ) noexcept;
    static std::tuple< int, int > position ( sdl2::MouseStateType type = sdl2::MouseStateType::None ) noexcept;
  };

  struct KB {
    static bool one ( ) noexcept;
    static bool two ( ) noexcept;
    static bool three ( ) noexcept;
    static bool four ( ) noexcept;
    static bool five ( ) noexcept;
    static bool six ( ) noexcept;
    static bool seven ( ) noexcept;
    static bool eight ( ) noexcept;
    static bool nine ( ) noexcept;
    static bool zero ( ) noexcept;
    static bool up ( ) noexcept;
    static bool down ( ) noexcept;
    static bool left ( ) noexcept;
    static bool right ( ) noexcept;
    static bool space ( ) noexcept;
    static bool w ( ) noexcept;
    static bool a ( ) noexcept;
    static bool d ( ) noexcept;
    static bool s ( ) noexcept;
  };

 public:
  GDApp ( );
  virtual ~GDApp ( ) noexcept = default;

 private:
  friend void event_loop ( GDApp *app );

 private:
  virtual void init ( ) = 0;
  virtual void destroy ( ) = 0;
  virtual void update ( double delta ) = 0;
  virtual void render ( double alpha ) = 0;

 private:
  virtual void onWindow ( const SDL_WindowEvent &event );
  virtual void onKeyboard ( const SDL_KeyboardEvent &event, const bool isPressed );
  virtual void onMouseButton ( const SDL_MouseButtonEvent &event, const bool isPressed );
  virtual void onMouseMotion ( const SDL_MouseMotionEvent &event );
  virtual void onMouseWheel ( const SDL_MouseWheelEvent &event );

 public:
  void run ( int argc, char *argv[] );
  double time ( ) const;

 public:
  static void stop ( );
  static System &system ( );
  static FileSystem &fs ( );
  static Window &window ( );
  static Renderer &renderer ( );
  static Texture &texture ( );
#if USE_SDL2_TTF
  static FontTTF &ttf ( );
#endif
  static Audio &audio ( );
  static Camera &camera ( );
  static Stats &stats ( );
  static Settings &settings ( );

 private:
  static bool stop_;
  static GDApp::Settings settings_;

  static System *systemPtr_;
  static FileSystem *fileSystemPtr_;
  static Window *windowPtr_;
  static Renderer *rendererPtr_;
  static Texture *texturePtr_;
#if USE_SDL2_TTF
  static FontTTF *ttfPtr_;
#endif
  static Audio *audioPtr_;
  static Camera *cameraPtr_;
  static Stats *statsPtr_;

 private:
  System system_;
  FileSystem fileSystem_;
  Window window_;
  Renderer renderer_;
  Texture texture_;
#if USE_SDL2_TTF
  FontTTF ttf_;
#endif
  Audio audio_;
  Camera camera_;
  Stats stats_;

 private:
  // Hertz, number of cycles per second
  std::int32_t frameCycle_{ 0 };
  // Current Frame rate
  double fps_{ 0.0 };
  // Total accumulated amount
  double accumulator_{ 0.0 };
  // 1 / 4 = .25
  double spiralOfDeath_{ 1.0 / 4.0 };
  // 1 / 60 = 0.0166667
  double fixedTickRate_{ 1.0 / 60.0 };

  double t0_{ 0.0 };
  double t1_{ 0.0 };
  double tFrameTime_{ 0.0 };
  double tFps_{ 0.0 };
  double tFpsDuration_{ 0.0 };
};

}  // namespace gd
