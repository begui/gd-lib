#pragma once

#include <cstdint>
#include <optional>
#include <variant>
#include <vector>

#include "gd/gd_sdl2.hh"

namespace gd {

using id_t = std::int32_t;
using cid_t = const id_t;

enum class BlendMode { None, Blend, Add, Mod };
struct Color : public SDL_Color {
 public:
  Color ( std::uint8_t r = 0, std::uint8_t g = 0, std::uint8_t b = 0, std::uint8_t a = 255 );
  void red ( std::uint8_t r );
  void green ( std::uint8_t g );
  void blue ( std::uint8_t b );
  void alpha ( std::uint8_t a );
  void rgba ( std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a );
  std::uint8_t red ( ) const;
  std::uint8_t green ( ) const;
  std::uint8_t blue ( ) const;
  std::uint8_t alpha ( ) const;
  std::uint32_t toInt ( ) const;

  operator std::int32_t ( ) const;
  bool operator== ( const Color& other ) const;
  bool operator!= ( const Color& other ) const;

  static const Color RED;
  static const Color GREEN;
  static const Color BLUE;
  static const Color BLACK;
  static const Color WHITE;
  static const Color CYAN;
  static const Color GRAY;
  static const Color MAROON;
  static const Color OLIVE;
  static const Color PURPLE;
  static const Color YELLOW;
};
using ColorOpt_t = std::optional< Color >;

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
template < typename T >
struct Point : public std::conditional< std::is_floating_point< T >::value, SDL_FPoint, SDL_Point >::type {
  static_assert ( std::is_same< std::int32_t, T >::value || std::is_same< float, T >::value,
                  "T must inherit from int or float " );

 public:
  Point ( T xx = 0, T yy = 0 )
      : std::conditional< std::is_floating_point< T >::value, SDL_FPoint, SDL_Point >::type{ xx, yy } {}
#else
template < typename T >
struct Point : public SDL_Point {
  static_assert ( std::is_same< std::int32_t, T >::value, "T must inherit from int" );

 public:
  Point ( T xx = 0, T yy = 0 ) : SDL_Point{ xx, yy } {}
#endif
  constexpr bool operator== ( const Point& other ) const;
  constexpr bool operator!= ( const Point& other ) const;
  constexpr bool operator< ( const Point& other ) const;
  void operator*= ( const Point& other );
  void operator/= ( const Point& other );
};

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
template < typename T >
struct Rect : public std::conditional< std::is_floating_point< T >::value, SDL_FRect, SDL_Rect >::type {
  static_assert ( std::is_same< std::int32_t, T >::value || std::is_same< float, T >::value,
                  "T must inherit from int or float " );

 public:
  Rect ( T xx = 0, T yy = 0, T ww = 0, T hh = 0 )
      : std::conditional< std::is_floating_point< T >::value, SDL_FRect, SDL_Rect >::type{ xx, yy, ww, hh } {}
#else
template < typename T >
struct Rect : public SDL_Rect {
  static_assert ( std::is_same< std::int32_t, T >::value, "T must inherit from int " );

 public:
  Rect ( T xx = 0, T yy = 0, T ww = 0, T hh = 0 ) : SDL_Rect{ xx, yy, ww, hh } {}
#endif
 public:
  constexpr bool operator== ( const Rect< T >& other ) const;
  constexpr bool operator!= ( const Rect< T >& other ) const;
};

template < typename T >
struct Circle {
  T x{ };
  T y{ };
  std::int32_t radius{ 1 };

 public:
  Circle ( ) {}
  Circle ( T xx, T yy, std::int32_t r ) : x ( xx ), y ( yy ), radius{ r } {}
};

/**
 * Rotation
 */
template < typename T >
struct Rotation {
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  static_assert ( std::is_base_of< SDL_Point, T >::value || std::is_base_of< SDL_FPoint, T >::value,
                  "T must inherit from SDL_Point or SDL_FPoint" );
#else
  static_assert ( std::is_base_of< SDL_Point, T >::value, "T must inherit from SDL_Point." );
#endif
  sdl2::FlipType flip{ sdl2::FlipType::None };
  double angle{ 0.0 };
  std::optional< T > angleCenter = std::nullopt;
};

using Pointi32_t = Point< std::int32_t >;
using Pointi32Opt_t = std::optional< Pointi32_t >;
using Recti32_t = Rect< std::int32_t >;
using Recti32Opt_t = std::optional< Recti32_t >;
using Rotationi32_t = Rotation< Pointi32_t >;
using Rotationi32Opt_t = std::optional< Rotationi32_t >;
using Circlei32_t = Circle< std::int32_t >;
using Circlei32Opt_t = std::optional< Circlei32_t >;
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
using Pointf_t = Point< float >;
using PointfOpt_t = std::optional< Pointf_t >;
using Rectf_t = Rect< float >;
using RectfOpt_t = std::optional< Rectf_t >;
using Rotationf_t = Rotation< Pointf_t >;
using RotationfOpt_t = std::optional< Rotationf_t >;

#endif

/**
 * Grid2d
 */
template < typename T >
class Grid2d final {
 public:
  explicit Grid2d ( std::int32_t x, std::int32_t y, const T& value = T{ } )  //
      : row_ ( x ), column_ ( y ), grid_ ( x * y, value ) {}

  explicit Grid2d ( const Pointi32_t& p, const T& value = T{ } )  //
      : Grid2d ( p.x, p.y, value ){ };

  auto getRowSize ( ) const { return row_; }
  auto getColumnSize ( ) const { return column_; }
  auto getSize ( ) const { return grid_.size ( ); }
  //  Always the Grid2d is row-major
  T& getCell ( std::int32_t x, std::int32_t y ) { return grid_.at ( x * column_ + y ); }
  T& getCell ( const Pointi32_t& p ) { return getCell ( p.x, p.y ); }

  T& operator[] ( const std::pair< std::int32_t, std::int32_t >& index ) {
    return getCell ( index.first, index.second );
  }

  const std::vector< T >& getVector ( ) const { return grid_; }

 private:
  std::int32_t row_;
  std::int32_t column_;
  std::vector< T > grid_;
};

struct TextureData final {
  friend class Texture;
  friend class Renderer;
  friend class FontTTF;

 public:
  auto getW ( ) const { return width; }
  auto getH ( ) const { return height; }

 private:
  std::int32_t width{ 0 };
  std::int32_t height{ 0 };

  sdl2::TextureUniqPtr texturePtr{ nullptr };
};
using TextureDataOpt_t = std::optional< TextureData >;

#if USE_SDL2_TTF

enum class TTFType : std::int32_t { Solid, Blended };
constexpr auto SOLID_V = static_cast< std::int32_t > ( TTFType::Solid );
constexpr auto BLENDED_V = static_cast< std::int32_t > ( TTFType::Blended );

struct FontTTFData {
  friend class Renderer;
  friend class FontTTF;

 public:
  struct FontChar {
    friend class Renderer;
    friend class FontTTF;

    FontChar ( const std::uint64_t cc ) { ch = cc; }

   public:
    auto getX ( ) const { return dstRect ? dstRect.value ( ).x : 0; }
    auto getY ( ) const { return dstRect ? dstRect.value ( ).y : 0; }
    auto getW ( ) const { return dstRect ? dstRect.value ( ).w : 0; }
    auto getH ( ) const { return dstRect ? dstRect.value ( ).h : 0; }

    void setX ( std::int32_t x ) {
      if ( dstRect ) dstRect.value ( ).x = x;
    }
    void setY ( std::int32_t y ) {
      if ( dstRect ) dstRect.value ( ).y = y;
    }
    void setW ( std::int32_t w ) {
      if ( dstRect ) dstRect.value ( ).w = w;
    }
    void setH ( std::int32_t h ) {
      if ( dstRect ) dstRect.value ( ).h = h;
    }

   public:
    Recti32Opt_t dstRect;
    Rotationi32Opt_t rotation;

   private:
    std::uint64_t ch;
  };

 public:
  std::int32_t margin;
  std::vector< FontChar > fontChars;
};
using FontTTFDataOpt_t = std::optional< FontTTFData >;
#endif

}  // namespace gd
