#pragma once

#include <memory>

namespace gd {
template<typename T>
class Pimpl final {
private:
  std::unique_ptr<T> impl_;

public:
  Pimpl() :
      impl_ { new T { } } {
  }
  template<typename ...Args>
  Pimpl(Args &&...args) :
      impl_ { new T { std::forward<Args>(args)... } } {

  }
  ~Pimpl() = default;

public:
  T* operator->() {
    return impl_.get();
  }
  T& operator*() {
    return *impl_.get();
  }
  T* operator->() const {
    return impl_.get();
  }
  T& operator*() const {
    return *impl_.get();
  }

};
}
