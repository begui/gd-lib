#pragma once

#include <set>
#include <type_traits>

namespace gd {
template < typename ENUM_T >
class EnumSelection {
  static_assert ( std::is_enum< ENUM_T >::value, "Must be an enum value" );

 public:
  EnumSelection ( ) { next ( ); }
  template < typename... Args >
  EnumSelection ( ENUM_T t1, Args&&... tn ) {
    add ( t1, tn... );
    next ( );
  }
  virtual ~EnumSelection ( ) = default;

 public:
  template < typename... Args >
  void add ( Args&&... tn ) {
    add ( tn... );
  }
  template < typename... Args >
  void add ( ENUM_T t1, Args&&... tn ) {
    add ( t1 );
    add ( tn... );
  }
  void add ( ENUM_T t1 ) { visibleSet.insert ( t1 ); }

  auto selected ( ) const { return currentSelected_; }
  auto previous ( ) const { return previousSelected_; }
  void next ( ) {
    previousSelected_ = currentSelected_;
    ++currentSelected_;
    if ( visibleSet.size ( ) > 0 ) {
      const auto isIn = visibleSet.find ( currentSelected_ ) != visibleSet.end ( );
      if ( !isIn ) {
        next ( );
      }
    }
  }
  void prev ( ) {
    previousSelected_ = currentSelected_;
    --currentSelected_;
    if ( visibleSet.size ( ) > 0 ) {
      const auto isIn = visibleSet.find ( currentSelected_ ) != visibleSet.end ( );
      if ( !isIn ) {
        prev ( );
      }
    }
  }

 public:
  static const bool enable = true;

 private:
  ENUM_T currentSelected_{ENUM_T::SELECTION_BEGIN};
  ENUM_T previousSelected_{ENUM_T::SELECTION_BEGIN};
  std::set< ENUM_T > visibleSet;
};

template < typename E >
typename std::enable_if< EnumSelection< E >::enable, E >::type  //
operator++ ( E& selection ) {
  using IntType = typename std::underlying_type< E >::type;
  selection = static_cast< E > ( static_cast< IntType > ( selection ) + 1 );
  if ( selection == E::SELECTION_END ) {
    selection = static_cast< E > ( 1 );
  }
  return selection;
}

template < typename E >
typename std::enable_if< EnumSelection< E >::enable, E >::type  //
operator-- ( E& selection ) {
  using IntType = typename std::underlying_type< E >::type;
  selection = static_cast< E > ( static_cast< IntType > ( selection ) - 1 );
  if ( selection == E::SELECTION_BEGIN ) {
    selection = static_cast< E > ( static_cast< IntType > ( E::SELECTION_END ) - 1 );
  }
  return selection;
}
}  // namespace gd
