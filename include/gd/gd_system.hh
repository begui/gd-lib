#pragma once

#if defined( __ANDROID__ )
#define GD_OS_ANDROID
#elif defined( __FreeBSD__ ) || defined( __NetBSD__ ) || defined( __OpenBSD__ ) || defined( __bsdi__ ) || \
    defined( __DragonFly__ ) || defined( _SYSTYPE_BSD )
#define GD_OS_BSD
#elif defined( __unix__ ) || defined( __unix ) || defined( __linux__ )
#define GD_OS_LINUX
#elif defined( __APPLE__ ) || defined( __MACH__ )
#define GD_OS_MAC
#elif defined( __WIN16 ) || defined( __WIN32 ) || defined( __WIN64 ) || defined( __WIN32__ ) || \
    defined( __TGD_OS_WIN__ ) || defined( __WINDOWS__ )
#define GD_OS_WIN
#else
#error Platform Unknown
#endif
enum class OS { Unknown, Android, Bsd, Linux, Mac, Mingw, Windows };
enum class COMPILER { Unknown, Clang, GNU, VCC, MingGW, Turbo };

namespace gd {

namespace os {
#if defined( GD_OS_ANDROID )
inline constexpr OS type{OS::Android};
#elif defined( GD_OS_BSD )
inline constexpr OS type{OS::Bsd};
#elif defined( GD_OS_LINUX )
inline constexpr OS type{OS::Linux};
#elif defined( GD_OS_MAC )
inline constexpr OS type{OS::Mac};
#elif defined( GD_OS_WIN )
inline constexpr OS type{OS::Windows};
#else
inline constexpr OS type{OS::Unknown};
#endif

}  // namespace os
namespace compiler {
#if defined( __clang__ )
inline constexpr COMPILER type{COMPILER::Clang};
#elif defined( __GNUC__ )
inline constexpr COMPILER type{COMPILER::GNU};
#elif defined( _MSC_VER )
inline constexpr COMPILER type{COMPILER::VCC};
#elif defined( __MINGW32__ ) || defined( __MINGW64__ )
inline constexpr COMPILER type{COMPILER::MingGW};
#elif defined( __TURBOC__ )
inline constexpr COMPILER type{COMPILER::Turbo};
#else
inline constexpr COMPILER type{COMPILER::Unknown};
#endif
}  // namespace compiler

class System final {
  friend class GDApp;

 private:
  void init ( );

 public:
  const char* os ( ) const;
  const char* compiler ( ) const;
  bool isCoreEnabled ( ) const;
  bool isVideoEnabled ( ) const;
  bool isAudioEnabled ( ) const;
  bool isEventsEnabled ( ) const;
  bool isFontEnabled ( ) const;
};
}  // namespace gd

