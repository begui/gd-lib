#pragma once

#include "gd/gd_app.hh"

enum class SandboxType {
  NONE,        //
  STARS,       //
  SQUARE_MAP,  //
  GRID2D_MAP,
  FONT
};

class Sandbox {
 public:
  virtual void init ( ) = 0;
  virtual void update ( double delta ) = 0;
  virtual void render ( double alpha ) = 0;
  virtual SandboxType type ( ) const = 0;
};

class FontSandbox final : public Sandbox {
 public:
  void init ( ) override;
  void update ( double delta ) override;
  void render ( double alpha ) override;
  SandboxType type ( ) const override { return SandboxType::FONT; }

#if USE_SDL2_TTF
  std::optional< gd::FontTTFData > text1;
  std::optional< gd::FontTTFData > text2;
  std::optional< gd::FontTTFData > textArray1;
#endif
};

class Grid2dMap final : public Sandbox {
 public:
  void init ( ) override;
  void update ( double delta ) override;
  void render ( double alpha ) override;
  SandboxType type ( ) const override { return SandboxType::GRID2D_MAP; }

 private:
  struct CellData {
    gd::Color color{ gd::Color::WHITE };
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
    gd::Rectf_t rect;
#else
    gd::Recti32_t rect;
#endif
    bool isMoveable;
  };

  gd::Grid2d< CellData > grid2d_{ 10, 10 };
};

class SquareMap final : public Sandbox {
 private:
  struct Square {
    gd::Pointi32_t curPos;
  };

 public:
  void init ( ) override;
  void update ( double delta ) override;
  void render ( double alpha ) override;
  SandboxType type ( ) const override { return SandboxType::SQUARE_MAP; }

 private:
  Square square_;
};

class Stars final : public Sandbox {
 private:
  struct Star {
    Star ( float xx, float yy, std::uint8_t color ) : x ( xx ), y ( yy ), color ( color ) {}
    float x{ 0 };
    float y{ 0 };
    std::uint8_t color{ 0x00 };
  };

 public:
  void init ( ) override;
  void update ( double delta ) override;
  void render ( double alpha ) override;
  SandboxType type ( ) const override { return SandboxType::STARS; }

 private:
  std::vector< Star > stars_;
};
