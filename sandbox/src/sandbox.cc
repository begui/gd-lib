#include "sandbox.hh"

#include <cmath>

const std::string str = "Hello world this is a not so really long long long string";
auto angle = 1.0;

/*****************************************************************
 *
 *
 *****************************************************************/

void FontSandbox::init ( ) {
#if USE_SDL2_TTF
  gd::GDApp::ttf ( ).setIdAndHeight ( gd::data::font::OpenSansRegular, 12 );
  gd::GDApp::ttf ( ).writeSolidString ( text1, str, gd::Color::RED );
  gd::GDApp::ttf ( ).writeBlendedString ( text2, str, gd::Color::RED );
  gd::GDApp::ttf ( ).writeSolidString ( textArray1, str, gd::Color::PURPLE );
#endif
}
void FontSandbox::update ( double delta ) {
#if USE_SDL2_TTF
  auto fontHeight = 0;

  /*
  text1->setY ( fontHeight );
  fontHeight += gd::GDApp::ttf ( ).getLineSkip ( );
  text2->setY ( fontHeight );
  */
  fontHeight += gd::GDApp::ttf ( ).getLineSkip ( );

  auto i{ 0 };
  if ( textArray1 ) {
    for ( auto &c : textArray1->fontChars ) {
      c.setY ( fontHeight * 2 + std::sin ( M_PI / 180 * ( angle + i * 15 ) ) * fontHeight );
      ++i;
    }
    angle += 1;
  }
#endif
}
void FontSandbox::render ( double alpha ) {
#if USE_SDL2_TTF
  gd::GDApp::renderer ( ).drawTTFFont ( text1 );
  gd::GDApp::renderer ( ).drawTTFFont ( text2 );
  gd::GDApp::renderer ( ).drawTTFFont ( textArray1 );
#endif

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  auto [ x, y, w, h ] = gd::GDApp::renderer ( ).viewportf ( );
  const auto halfHeight = h / 2.f;
#else
  auto [ x, y, w, h ] = gd::GDApp::renderer ( ).viewport ( );
  const auto halfHeight = static_cast< int > ( h / 2 );
#endif

  gd::GDApp::renderer ( ).drawRectFill (
      {
          0,
          halfHeight,
          10,
          10,
      },
      gd::Color::CYAN );
  gd::GDApp::renderer ( ).drawLineH ( halfHeight, 15, 35, gd::Color::PURPLE );
  gd::GDApp::renderer ( ).drawLineV ( 45, halfHeight, 190, gd::Color::OLIVE );
  // gd::GDApp::renderer ( ).drawCircle ( {65, static_cast< int > ( halfHeight ), 10}, gd::Color::MAROON );
  gd::GDApp::renderer ( ).drawCircle ( { 65, static_cast< int > ( halfHeight ), 10 }, gd::Color::MAROON );
}

/*****************************************************************
 *
 *
 *****************************************************************/

void Grid2dMap::init ( ) {
  constexpr auto size{ 32 };
  for ( auto i{ 0 }; i < grid2d_.getRowSize ( ); ++i ) {
    for ( auto j{ 0 }; j < grid2d_.getColumnSize ( ); ++j ) {
      grid2d_[ { i, j } ].isMoveable = true;
      grid2d_[ { i, j } ].rect.w = size - 5;
      grid2d_[ { i, j } ].rect.h = size - 5;
      grid2d_[ { i, j } ].rect.x = i * size;
      grid2d_[ { i, j } ].rect.y = j * size;
    }
  }
}
void Grid2dMap::update ( double delta ) {}
void Grid2dMap::render ( double alpha ) {
  const auto &vec = grid2d_.getVector ( );
  for ( const auto &v : vec ) {
    gd::GDApp::renderer ( ).drawRect ( v.rect, v.color );
  }
}

/*****************************************************************
 *
 *
 *****************************************************************/
