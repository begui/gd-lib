#include <iostream>

#include "sandbox.hh"

constexpr auto STAR_SIZE{ 1000 };

const auto calculate_initial_point = [] ( auto dim ) {  //
  return 1.0 * ( rand ( ) % dim );
};

const auto calculate_new_point = [] ( auto a, auto dim, auto color ) {
  return ( a - dim / 2 ) * ( color * 0.00001 + 1 ) + dim / 2;
};

void Stars::init ( ) {
   auto [ width, height ] = gd::GDApp::renderer ( ).size ( );
  //auto [ width, height ] = gd::GDApp::window ( ).size ( );
  for ( auto i{ 0 }; i < STAR_SIZE; ++i ) {
    const auto x = calculate_initial_point ( width );
    const auto y = calculate_initial_point ( height );
    stars_.emplace_back ( x, y, 0x00 );
  }
}

void Stars::update ( double delta ) {
   auto [ width, height ] = gd::GDApp::renderer ( ).size ( );
  //auto [ width, height ] = gd::GDApp::window ( ).size ( );


  for ( auto &star : stars_ ) {
    if ( star.color < 0xff ) {
      star.color += 1;
    }
    star.x = calculate_new_point ( star.x, width, star.color );
    star.y = calculate_new_point ( star.y, height, star.color );
    // If we are out of screen boundry
    if ( star.x < 0 || star.x > width || star.y < 0 || star.y > height ) {
      // create a new star
      star.x = calculate_initial_point ( width );
      star.y = calculate_initial_point ( height );
      star.color = 0x00;
    }
  }
}

void Stars::render ( double alpha ) {
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  for ( const auto &star : stars_ ) {
    gd::GDApp::renderer ( ).drawPoint ( gd::Pointf_t{ star.x, star.y }, { star.color, star.color, 0xff } );
  }

#else
  for ( const auto &star : stars_ ) {
    const auto x = static_cast< int > ( star.x );
    const auto y = static_cast< int > ( star.y );
    gd::GDApp::renderer ( ).drawPoint ( gd::Pointi32_t{ x, y }, { star.color, star.color, 0xff } );
  }
#endif
}
