#include "sandbox.hh"

constexpr auto BOX_SIZE{ 32 };
constexpr auto BOX_MAX{ 2 };
constexpr static auto VELOCITY{ 250.0f / 1.0f };

constexpr static auto GEN_SquareMap = 1;

void SquareMap::init ( ) {
  const auto size = BOX_SIZE;
  auto func = std::bind ( [ this, size ] {
    for ( auto i{ 0 }; i < BOX_MAX; i++ ) {
      for ( auto j{ 0 }; j < BOX_MAX; j++ ) {
        if ( ( i % 2 == 0 ) != ( j % 2 == 0 ) ) {
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
          auto ii = static_cast< float > ( i ) * size;
          auto jj = static_cast< float > ( j ) * size;
#else
          auto ii = i * size;
          auto jj = j * size;
#endif
          gd::GDApp::renderer ( ).drawRectFill ( { ii, jj, size, size }, gd::Color::GRAY );
        }
      }
    }
  } );

  const auto gridsize = BOX_SIZE * BOX_MAX;
  gd::GDApp::texture ( ).loadTarget ( GEN_SquareMap, gridsize, gridsize, func );
}

void SquareMap::update ( double delta ) {
  if ( gd::GDApp::KB::one ( ) ) {
    // gd::GDApp::camera ( ).shake ( 1 );
  }
}

void SquareMap::render ( double alpha ) {
  const auto [ dimW, dimH ] = gd::GDApp::renderer ( ).size ( );

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  auto centerX = ( dimW / 2.0f - BOX_SIZE / 2.0f );
  auto centerY = ( dimH / 2.0f - BOX_SIZE / 2.0f );
#else
  auto centerX = static_cast< int > ( dimW / 2.0f - BOX_SIZE / 2.0f );
  auto centerY = static_cast< int > ( dimH / 2.0f - BOX_SIZE / 2.0f );
#endif

  auto [ camX, camY ] = gd::GDApp::camera ( ).cameraCoords ( );
  auto [ camXOff, camYOff ] = gd::GDApp::camera ( ).cameraCoordsOffset ( );
  auto [ toWX, toWY ] = gd::GDApp::camera ( ).toWorldCoords ( centerX, centerY );
  auto [ toSX, toSY ] = gd::GDApp::camera ( ).toScreenCoords ( centerX, centerY );

  gd::GDApp::renderer ( ).drawTexture ( GEN_SquareMap );
  // Draw Square
  // Set middle of screen
  gd::GDApp::renderer ( ).drawRectFill ( { centerX, centerY, BOX_SIZE, BOX_SIZE }, gd::Color::RED );
}
