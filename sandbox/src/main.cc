#include <cstdlib>

#include "gd/gd_app.hh"
#include "sandbox.hh"

class SandboxApp : public gd::GDApp {
 public:
  void init ( ) override;
  void destroy ( ) override;
  void update ( double delta ) override;
  void render ( double alpha ) override;

 public:
  void onKeyboard ( const SDL_KeyboardEvent& event, bool isPressed ) override;
  // void onMouseButton ( const SDL_MouseButtonEvent& event, bool isPressed ) override;
  // void onMouseMotion ( const SDL_MouseMotionEvent& event ) override;
  // void onMouseWheel ( const SDL_MouseWheelEvent& event ) override;

 private:
  SandboxType type_{ SandboxType::NONE };
  Stars stars_;
  SquareMap squareMap_;
  Grid2dMap grid2dMap_;
  FontSandbox fontSandbox_;
  Sandbox* ptr_{ nullptr };
};

int main ( int argc, char* argv[] ) {
  SandboxApp game;

  //  game.settings ( ).screen.width = 1200;
  //  game.settings ( ).screen.height = 1200 * 16 / 25;

  game.settings ( ).window.width = 1280;
  game.settings ( ).window.height = 720;

  game.settings ( ).window.resizable = true;
  game.settings ( ).window.fullscreen = false;

  game.settings ( ).renderer.integerScale = false;
  game.settings ( ).renderer.width = 640;
  game.settings ( ).renderer.height = 360;
  game.settings ( ).renderer.mode = gd::BlendMode::Blend;
  game.settings ( ).renderer.vsync = true;

  game.settings ( ).printInfo = true;

#if USE_SDL2_TTF
  game.settings ( ).font.fonts.emplace_back ( gd::data::font::OpenSansLight, 12 );
  game.settings ( ).font.fonts.emplace_back ( gd::data::font::OpenSansRegular, 24 );
#endif

  sdl2::log::set_all_priorities ( sdl2::log::PriorityType::Debug );
  game.run ( argc, argv );

  return EXIT_SUCCESS;
}

void SandboxApp::init ( ) {
  window ( ).title ( "Sandbox" );
  stars_.init ( );
  squareMap_.init ( );
  grid2dMap_.init ( );
  fontSandbox_.init ( );
}
void SandboxApp::destroy ( ) {}
void SandboxApp::update ( double delta ) {
  // TODO: pass framecycle
  if ( ptr_ != nullptr ) {
    ptr_->update ( delta );
  }

  if ( gd::GDApp::KB::up ( ) ) {
    gd::GDApp::camera ( ).translateY ( delta );
  }

  if ( gd::GDApp::KB::down ( ) ) {
    gd::GDApp::camera ( ).translateY ( -delta );
  }
  if ( gd::GDApp::KB::left ( ) ) {
    gd::GDApp::camera ( ).translateX ( delta );
  }
  if ( gd::GDApp::KB::right ( ) ) {
    gd::GDApp::camera ( ).translateX ( -delta );
  }
}
void SandboxApp::render ( double alpha ) {
  if ( ptr_ != nullptr ) {
    ptr_->render ( alpha );
  }
}

void SandboxApp::onKeyboard ( const SDL_KeyboardEvent& event, bool isPressed ) {
  auto keyCode = event.keysym.sym;
  switch ( keyCode ) {
    case SDLK_ESCAPE:
      stop ( );
      break;
    case SDLK_F1:
      ptr_ = nullptr;
      break;
    case SDLK_F2:
      ptr_ = &stars_;
      break;
    case SDLK_F3:
      ptr_ = &squareMap_;
      break;
    case SDLK_F4:
      ptr_ = &grid2dMap_;
      break;
    case SDLK_F5:
      ptr_ = &fontSandbox_;
      break;
    case SDLK_BACKQUOTE:
      if ( isPressed ) {
        stats ( ).toggle ( );
      }
      break;
    case SDLK_LEFT:
      break;
    case SDLK_RIGHT:
      break;
    case SDLK_DOWN:
      break;
    case SDLK_UP:
      break;
    case SDLK_f:
      if ( isPressed ) {
        settings ( ).window.fullscreen = !settings ( ).window.fullscreen;
        window ( ).fullscreen ( settings ( ).window.fullscreen );
      }
      break;
    default:
      break;
  }
}
