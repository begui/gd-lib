#include "gd/gd_renderer.hh"

#include <array>
#include <iostream>
#include <optional>
#include <set>
#include <unordered_map>
#include <vector>

#include "gd/gd_app.hh"

namespace gd {
/*
 * =====================================================================================
 *        Class: Impl
 *  Description: Impl definitions
 * =====================================================================================
 */

class Window::Impl final {
  friend class Window;

 public:
  inline operator SDL_Window* ( ) { return window_.get ( ); }
  inline operator SDL_Window* ( ) const { return window_.get ( ); }

 private:
  sdl2::WindowUniqPtr window_{ nullptr };
};
Window::Window ( ) {}
Window::~Window ( ) {}
Window::Impl& Window::impl ( ) { return *impl_; }
Window::Impl& Window::impl ( ) const { return *impl_; }
//
//
//
class Texture::Impl final {
  friend class Texture;
  friend class Renderer;

 private:
  // TODO: look into implementing something else that will manage the id's for you
  std::unordered_map< std::int32_t, sdl2::TextureUniqPtr > textures_;
};

Texture::Texture ( ) {}
Texture::~Texture ( ) {}  // Required for PIMPL
Texture::Impl& Texture::impl ( ) { return *impl_; }
Texture::Impl& Texture::impl ( ) const { return *impl_; }
//
//
//
#if USE_SDL2_TTF
class FontTTF::Impl final {
  friend class FontTTF;

 private:
  struct TTFFontGlyph {
    std::int32_t width, height;
    std::int32_t xMin, xMax;
    std::int32_t yMin, yMax;
    std::int32_t advance;
    TextureDataOpt_t textureData{ std::nullopt };
  };

  struct TTFFontData {
    const TTFType type;
    const std::int32_t height;
    const std::int32_t lineHeight;
    const std::int32_t ascent;
    const std::int32_t descent;
    std::unordered_map< std::uint64_t, TTFFontGlyph > glyphs;
  };

  struct TTFFont {
    TTFFont ( sdl2::TTFFontUniqPtr ptr ) : fontPtr ( std::move ( ptr ) ) {}
    //
    sdl2::TTFFontUniqPtr fontPtr{ nullptr };

    std::array< TTFFontData, 2 > fontData{ {
        { TTFType::Solid, 0, 0, 0, 0, {} },   //
        { TTFType::Blended, 0, 0, 0, 0, {} }  //
    } };
  };

  struct TTFFontCache {
    TTFFontCache ( sdl2::RWopsUniqPtr ptr ) : rwopsTTF ( std::move ( ptr ) ) {}
    //
    sdl2::RWopsUniqPtr rwopsTTF{ nullptr };
    // Key=PointSize,Value=FontCache::TTFFont
    std::unordered_map< std::int32_t, TTFFont > fonts;
  };

 public:
  TTFFont& getCurrentTTFFont ( ) {
    const auto fontCacheRef = fontCache_.find ( currentId_ );
    const auto ttfFont = fontCacheRef->second.fonts.find ( pointSize_ );
    return ttfFont->second;
  }

 public:
  // TODO: remove this
  inline operator TTF_Font* ( ) {
    if ( const auto fontCacheIter = fontCache_.find ( currentId_ ); fontCacheIter != fontCache_.end ( ) ) {
      if ( const auto fontIter = fontCacheIter->second.fonts.find ( pointSize_ );
           fontIter != fontCacheIter->second.fonts.end ( ) ) {
        return fontIter->second.fontPtr.get ( );
      }
    }
    return nullptr;
  }

 private:
  std::unordered_map< gd::id_t, TTFFontCache > fontCache_;
  id_t currentId_{ 0 };
  std::int32_t pointSize_{ 10 };
};

FontTTF::FontTTF ( ) {}
FontTTF::~FontTTF ( ) {}  // Required for PIMPL
FontTTF::Impl& FontTTF::impl ( ) { return *impl_; }
FontTTF::Impl& FontTTF::impl ( ) const { return *impl_; }

#endif
//
//
//
class Renderer::Impl final {
  friend class Renderer;

 public:
  inline operator SDL_Renderer* ( ) { return renderer_.get ( ); }
  inline operator SDL_Renderer* ( ) const { return renderer_.get ( ); }

 private:
  sdl2::RendererUniqPtr renderer_{ nullptr };
  Color clearColor_{ 0x00, 0x00, 0x00, 0x00 };
  bool integerScale_{ true };
};

Renderer::Renderer ( ) {}
Renderer::~Renderer ( ) {}  // Required for PIMPL
Renderer::Impl& Renderer::impl ( ) { return *impl_; }
Renderer::Impl& Renderer::impl ( ) const { return *impl_; }

/*
 * =====================================================================================
 *  Functions:
 * =====================================================================================
 */
auto translate_blend_mode = [] ( BlendMode mode ) {
  switch ( mode ) {
    case BlendMode::None:
      return sdl2::BlendmodeType::None;
      break;
    case BlendMode::Blend:
      return sdl2::BlendmodeType::Blend;
      break;
    case BlendMode::Add:
      return sdl2::BlendmodeType::Add;
      break;
    case BlendMode::Mod:
      return sdl2::BlendmodeType::Mod;
      break;
  }
  return sdl2::BlendmodeType::None;
};
auto translate_sdl2_blend_mode = [] ( sdl2::BlendmodeType mode ) {
  switch ( mode ) {
    case sdl2::BlendmodeType::None:
      return BlendMode::None;
      break;
    case sdl2::BlendmodeType::Blend:
      return BlendMode::Blend;
      break;
    case sdl2::BlendmodeType::Add:
      return BlendMode::Add;
      break;
    case sdl2::BlendmodeType::Mod:
      return BlendMode::Mod;
      break;
  }
  return BlendMode::None;
};
/*
 * =====================================================================================
 *        Class:  Window
 *  Description:
 * =====================================================================================
 */
void Window::init ( ) {
  const auto& settings = gd::GDApp::settings ( );
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  SDL_SetHint ( SDL_HINT_RENDER_BATCHING, settings.window.rendererBatching ? "1" : "0" );
#endif
  if constexpr ( gd::os::type == OS::Windows && gd::compiler::type != COMPILER::VCC ) {
    SDL_SetHint ( SDL_HINT_RENDER_DRIVER, "opengl" );
  }

  std::uint32_t flags{ 0x00000000 };
  flags |= ( settings.window.fullscreen ? SDL_WINDOW_FULLSCREEN : 0x00000000 );
  flags |= ( settings.window.resizable ? SDL_WINDOW_RESIZABLE : 0x00000000 );
  flags |= ( settings.window.maximized ? SDL_WINDOW_MAXIMIZED : 0x00000000 );

  impl ( ).window_ = sdl2::create_window< decltype ( impl ( ).window_ ) > (  //
      { settings.window.width, settings.window.height },                     //
      { SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED },                    //
      flags                                                                  //
  );
  if ( nullptr == impl ( ).window_ ) {
    throw std::runtime_error ( "Couldn't create a window" );
  }
}
void Window::printInfo ( ) const { sdl2::print_window_info ( impl ( ) ); }

std::uint32_t Window::windowId ( ) const { return sdl2::get_window_id ( impl ( ) ); }
std::int32_t Window::refreshRate ( ) const { return sdl2::get_window_refresh_rate ( impl ( ) ); }
std::tuple< int, int > Window::size ( ) const { return sdl2::get_window_size ( impl ( ) ); }
std::tuple< int, int > Window::position ( ) const { return sdl2::get_window_position ( impl ( ) ); }
float Window::opacity ( ) const { return sdl2::get_window_opacity ( impl ( ) ); }

Window& Window::fullscreen ( bool flag ) {
  sdl2::set_window_state ( impl ( ), flag ? sdl2::WindowStateType::Fullscreen : sdl2::WindowStateType::None );
  return *this;
}
Window& Window::title ( const std::string& title ) {
  SDL_SetWindowTitle ( impl ( ), title.c_str ( ) );
  return *this;
}
Window& Window::position ( const int x, const int y ) {
  sdl2::set_window_position ( impl ( ), x, y );
  return *this;
}
Window& Window::opacity ( float op ) {
  sdl2::set_window_opacity ( impl ( ), op );
  return *this;
}
Window& Window::show ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Show );
  return *this;
}
Window& Window::hide ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Hide );
  return *this;
}
Window& Window::raise ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Raise );
  return *this;
}
Window& Window::maximize ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Maximize );
  return *this;
}
Window& Window::minimize ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Minimize );
  return *this;
}
Window& Window::restore ( ) {
  sdl2::set_window_state ( impl ( ), sdl2::WindowStateType::Restore );
  return *this;
}
/*
 * =====================================================================================
 *        Class:Texture
 *  Description:
 * =====================================================================================
 */

bool Texture::unload ( cid_t id ) noexcept {
  auto rtn{ false };
  if ( const auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    impl ( ).textures_.erase ( iter );
    rtn = true;
  }
  return rtn;
}

void Texture::color ( cid_t id, const Color& color ) const noexcept {
  if ( const auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    sdl2::set_color_mod ( iter->second.get ( ), color );
  }
}

ColorOpt_t Texture::color ( cid_t id ) const noexcept {
  if ( const auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    const auto [ r, g, b, a ] = sdl2::get_color_mod ( iter->second.get ( ) );
    return Color{ r, g, b, a };
  }
  return std::nullopt;
}

void Texture::blendMode ( cid_t id, const BlendMode mode ) const noexcept {
  if ( const auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    sdl2::set_blend_mode ( iter->second.get ( ), translate_blend_mode ( mode ) );
  }
}

BlendMode Texture::blendMode ( cid_t id ) const noexcept {
  if ( const auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    return translate_sdl2_blend_mode ( sdl2::get_blend_mode ( iter->second.get ( ) ) );
  }
  return BlendMode::None;
}

void Texture::alpha ( cid_t id, const std::uint8_t alpha ) const noexcept {
  if ( auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    sdl2::set_alpha_mod ( iter->second.get ( ), alpha );
  }
}

std::uint8_t Texture::alpha ( cid_t id ) const noexcept {
  if ( auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    return sdl2::get_alpha_mod ( iter->second.get ( ) );
  }
  return 0;
}

std::tuple< int, int > Texture::size ( cid_t id ) const noexcept {
  if ( auto iter = impl ( ).textures_.find ( id ); iter != impl ( ).textures_.end ( ) ) {
    return sdl2::get_texture_dimension ( iter->second.get ( ) );
  }
  return std::make_tuple ( 0, 0 );
}
TextureDataOpt_t Texture::create ( const std::string& filename ) const noexcept {
  bool rtn = gd::GDApp::fs ( ).fileExists ( filename );
  if ( rtn ) {
    if ( auto [ resource, size ] = gd::GDApp::fs ( ).load ( filename ); resource != nullptr && size > 0LL ) {
      TextureData texture;
      texture.texturePtr =
          sdl2::create_texture< sdl2::TextureUniqPtr > ( gd::GDApp::renderer ( ).impl ( ), resource.get ( ), size );
      auto [ w, h ] = sdl2::get_texture_dimension ( texture.texturePtr.get ( ) );
      texture.width = w;
      texture.height = h;
      sdl2::log::debug ( sdl2::log::CategoryType::Application, "Texture Created" );
      return texture;
    }
  }
  return std::nullopt;
}

bool Texture::load ( cid_t id, const std::string& filename ) noexcept {
  bool rtn{ false };
  if ( auto texture = create ( filename ); texture ) {
    impl ( ).textures_[ id ] = std::move ( texture->texturePtr );
    rtn = true;
  }
  return rtn;
}

TextureDataOpt_t Texture::createTarget ( const std::int32_t w, const std::int32_t h ) const noexcept {
  sdl2::log::debug ( sdl2::log::CategoryType::Application, "Texture Target Created" );
  TextureData texture;
  texture.width = w;
  texture.height = h;
  texture.texturePtr = sdl2::create_texture< sdl2::TextureUniqPtr > ( gd::GDApp::window ( ).impl ( ),    //
                                                                      gd::GDApp::renderer ( ).impl ( ),  //
                                                                      sdl2::TextureAccessType::Target, w, h );
  return texture;
}

bool Texture::loadTarget ( cid_t id, const std::int32_t w, const std::int32_t h,
                           std::function< void ( ) >&& func ) noexcept {
  bool rtn{ false };
  if ( auto texture = createTarget ( w, h ); texture && texture->texturePtr ) {
    impl ( ).textures_[ id ] = std::move ( texture->texturePtr );
    if ( func ) {
      sdl2::render_to_texture ( func, gd::GDApp::renderer ( ).impl ( ), impl ( ).textures_[ id ].get ( ) );
    }
    rtn = true;
  }
  return rtn;
}

TextureDataOpt_t Texture::createStream ( const std::int32_t w, const std::int32_t h ) const noexcept {
  sdl2::log::debug ( sdl2::log::CategoryType::Application, "Texture Stream Created" );
  TextureData texture;
  texture.width = w;
  texture.height = h;
  texture.texturePtr = sdl2::create_texture< sdl2::TextureUniqPtr > ( gd::GDApp::window ( ).impl ( ),    //
                                                                      gd::GDApp::renderer ( ).impl ( ),  //
                                                                      sdl2::TextureAccessType::Streaming, w, h );
  return texture;
}

bool Texture::updateStream ( TextureData& textureData,                                //
                             std::function< void ( std::uint32_t* pixels ) >&& func,  //
                             const Recti32_t* rect ) noexcept {
  bool rtn{ false };
  if ( textureData.texturePtr ) {
    sdl2::render_to_pixel ( func, textureData.texturePtr.get ( ), rect );
    rtn = true;
  }
  return rtn;
}

bool Texture::updateStream ( cid_t id, std::function< void ( std::uint32_t* pixels ) >&& func,
                             const Recti32_t* rect ) noexcept {
  bool rtn{ false };
  if ( func ) {
    auto iter = impl ( ).textures_.find ( id );
    if ( iter != impl ( ).textures_.end ( ) ) {
      sdl2::render_to_pixel ( func, iter->second.get ( ), rect );
      rtn = true;
    }
  }
  return rtn;
}

bool Texture::loadStream ( cid_t id, const std::int32_t w, const std::int32_t h ) noexcept {
  bool rtn{ false };
  if ( auto texture = createStream ( w, h ); texture && texture->texturePtr ) {
    impl ( ).textures_[ id ] = std::move ( texture->texturePtr );
    rtn = true;
  }
  return rtn;
}

/*
 * =====================================================================================
 *        Class: FontTTF
 *  Description:
 * =====================================================================================
 */

#if USE_SDL2_TTF
void FontTTF::init ( ) {
  //
  if ( !sdl2::isTTFEnabled ( ) ) {
    throw std::runtime_error ( "TTF Font is not initialized." );
  }
  const auto& settings = gd::GDApp::settings ( );
  const auto& fonts = settings.font.fonts;
  for ( auto&& font : fonts ) {
    loadInternal ( font.id, font.height );
  }
}

void FontTTF::setId ( cid_t id ) { impl ( ).currentId_ = id; }
void FontTTF::setHeight ( const std::int32_t height ) { impl ( ).pointSize_ = height; }
void FontTTF::setIdAndHeight ( cid_t id, std::int32_t height ) {
  setId ( id );
  setHeight ( height );
}

std::int32_t FontTTF::getLineSkip ( ) const {
  const TTF_Font* font = gd::GDApp::ttf ( ).impl ( );
  return sdl2::ttf::get_height_in_font ( font );
}

bool FontTTF::hasGlyph ( std::uint16_t ch ) const {
  const TTF_Font* font = gd::GDApp::ttf ( ).impl ( );
  return 0 != sdl2::ttf::has_glyph ( font, ch );
}

const std::tuple< std::int32_t, std::int32_t > FontTTF::getIdAndHeight ( ) const {
  return std::make_tuple ( impl ( ).currentId_, impl ( ).pointSize_ );
}

bool FontTTF::load ( [[maybe_unused]] const std::string& file, [[maybe_unused]] cid_t id,
                     [[maybe_unused]] std::int32_t fontHeight ) {
  auto rtn{ false };
  auto rwOpsIter = impl ( ).fontCache_.find ( id );
  if ( rwOpsIter == impl ( ).fontCache_.end ( ) ) {
    if ( auto [ resource, size ] = gd::GDApp::fs ( ).load ( file ); resource != nullptr && size > 0LL ) {
      auto [ iter, result ] = impl ( ).fontCache_.try_emplace ( id, sdl2::rw_FromConstMem ( resource.get ( ), size ) );
      rwOpsIter = iter;
    }
  }
  auto& fonts = rwOpsIter->second.fonts;
  auto fontIter = fonts.find ( fontHeight );
  if ( fontIter == fonts.end ( ) ) {
    auto& fontRwops = rwOpsIter->second.rwopsTTF;
    fonts.try_emplace ( fontHeight, sdl2::ttf::load< sdl2::TTFFontUniqPtr > ( fontRwops.get ( ), fontHeight, 0 ) );
    setIdAndHeight ( id, fontHeight );
    rtn = true;
  }

  return rtn;
}

bool FontTTF::loadInternal ( [[maybe_unused]] cid_t id, [[maybe_unused]] const std::int32_t fontHeight ) {
  bool rtn{ false };

  auto [ fontData, fontDataSize ] = gd::data::font::getFont ( id );
  auto rwOpsIter = impl ( ).fontCache_.find ( id );
  if ( rwOpsIter == impl ( ).fontCache_.end ( ) ) {
    auto [ iter, result ] = impl ( ).fontCache_.try_emplace ( id, sdl2::rw_FromConstMem ( fontData, fontDataSize ) );
    rwOpsIter = iter;
  }

  auto& fonts = rwOpsIter->second.fonts;
  auto fontIter = fonts.find ( fontHeight );
  if ( fontIter == fonts.end ( ) ) {
    auto& fontRwops = rwOpsIter->second.rwopsTTF;
    fonts.try_emplace ( fontHeight, sdl2::ttf::load< sdl2::TTFFontUniqPtr > ( fontRwops.get ( ), fontHeight, 0 ) );
    setIdAndHeight ( id, fontHeight );
    rtn = true;
  }

  return rtn;
}

void FontTTF::writeCachedString ( FontTTFDataOpt_t& fontDataStr,  //
                                  const std::string& text,        //
                                  const TTFType type,             //
                                  const Color& color ) const {
  if ( !fontDataStr.has_value ( ) ) {
    fontDataStr = std::make_optional< FontTTFData > ( );
  }
  fontDataStr->fontChars.clear ( );

  auto& ttfFont = impl ( ).getCurrentTTFFont ( );
  const auto fontData = &ttfFont.fontData[ type == TTFType::Solid ? SOLID_V : BLENDED_V ];
  const auto ttfFontPtr = ttfFont.fontPtr.get ( );
  for ( const auto& c : text ) {
    const std::uint16_t ch = static_cast< std::uint16_t > ( c );
    const std::uint64_t chWithColor = ( ch * 1000000000000 ) + color.toInt ( );

    auto fontDataCharIter = fontData->glyphs.find ( chWithColor );
    if ( fontDataCharIter == fontData->glyphs.end ( ) ) {
      if ( gd::GDApp::ttf ( ).hasGlyph ( ch ) ) {
        auto [ iter, success ] = fontData->glyphs.try_emplace ( chWithColor );
        if ( success ) {
          fontDataCharIter = iter;
          // TOOD: add data to fontDataCharIter
          std::uint16_t tmpch[ 2 ]{ ch, '\0' };
          auto fontSurface = sdl2::ttf::render< sdl2::SurfaceUniqPtr > (  //
              ttfFontPtr,                                                 //
              tmpch,                                                      //
              color,                                                      //
              color,                                                      //
              sdl2::ttf::RenderModeType::Solid );

          fontDataCharIter->second.textureData = std::make_optional< TextureData > ( );
          fontDataCharIter->second.textureData->texturePtr = sdl2::surface_to_texture< sdl2::TextureUniqPtr > (  //
              gd::GDApp::renderer ( ).impl ( ),                                                                  //
              std::move ( fontSurface ) );

          const auto [ tW, tH ] =
              sdl2::get_texture_dimension ( fontDataCharIter->second.textureData->texturePtr.get ( ) );
          fontDataCharIter->second.textureData->width = tW;
          fontDataCharIter->second.textureData->height = tH;
          const auto [ minX, maxX, minY, maxY, advance ] = sdl2::ttf::get_glyph_metrics ( ttfFontPtr, ch );
          fontDataCharIter->second.xMin = minX;
          fontDataCharIter->second.xMax = maxX;
          fontDataCharIter->second.yMin = minY;
          fontDataCharIter->second.yMax = maxY;
          fontDataCharIter->second.advance = advance;
          const auto [ w, h ] = sdl2::ttf::get_text_size ( ttfFontPtr, &tmpch[ 0 ] );
          fontDataCharIter->second.width = w;
          fontDataCharIter->second.height = h;
        }
      }
    }
    if ( fontDataCharIter != fontData->glyphs.end ( ) ) {
      auto& iter = fontDataStr->fontChars.emplace_back ( chWithColor );
      iter.dstRect = { 0, 0, fontDataCharIter->second.width, fontDataCharIter->second.height };
    }
  }
}
void FontTTF::writeSolidString ( FontTTFDataOpt_t& fontData, const std::string& text, const Color& fg ) const {
  writeCachedString ( fontData, text, TTFType::Solid, fg );
}

void FontTTF::writeBlendedString ( FontTTFDataOpt_t& fontData, const std::string& text, const Color& fg ) const {
  writeCachedString ( fontData, text, TTFType::Blended, fg );
}

#endif

/*
 * =====================================================================================
 *        Class:  Renderer
 *  Description:
 * =====================================================================================
 */

void Renderer::init ( const Window& window ) {
  const auto& settings = gd::GDApp::settings ( );

  std::uint32_t flags{ 0x00000000 };
  flags |= SDL_RENDERER_ACCELERATED;
  flags |= ( settings.renderer.renderToTexture ? SDL_RENDERER_TARGETTEXTURE : 0x00000000 );
  flags |= ( settings.renderer.vsync ? SDL_RENDERER_PRESENTVSYNC : 0x00000000 );
  impl ( ).renderer_ = sdl2::create_renderer< decltype ( impl ( ).renderer_ ) > (  //
      window.impl ( ),                                                             //
      translate_blend_mode ( settings.renderer.mode ),                             //
      flags                                                                        //
  );
  if ( nullptr == impl ( ).renderer_ ) {
    throw std::runtime_error ( "Couldn't create a renderer" );
  }

  impl ( ).integerScale_ = settings.renderer.integerScale;
  const auto w = settings.renderer.width > 0 ? settings.renderer.width : settings.window.width;
  const auto h = settings.renderer.height > 0 ? settings.renderer.height : settings.window.height;
  size ( w, h );
}
void Renderer::printInfo ( ) const { sdl2::print_renderer_info ( impl ( ) ); }
void Renderer::clear ( ) const { sdl2::clear ( impl ( ), impl ( ).clearColor_ ); }
void Renderer::swap ( ) const { sdl2::swap ( impl ( ) ); }
void Renderer::clearColor ( const Color& color ) { impl ( ).clearColor_ = color; }

std::tuple< int, int > Renderer::size ( ) const {
  auto [ x, y ] = sdl2::get_render_logical_size ( impl ( ) );
  if ( x == 0 || y == 0 ) {
    return gd::GDApp::window ( ).size ( );
  }
  return { x, y };
}
void Renderer::size ( const std::int32_t w, const std::int32_t h ) const {
  sdl2::set_render_logical_size ( impl ( ), w, h, impl ( ).integerScale_ );
}

std::tuple< float, float > Renderer::sizef ( ) const {
  auto [ x, y ] = size ( );
  if ( x == 0 || y == 0 ) {
    auto [ wx, wy ] = gd::GDApp::window ( ).size ( );
    return { static_cast< float > ( wx ), static_cast< float > ( wy ) };
  }
  return { static_cast< float > ( x ), static_cast< float > ( y ) };
}
void Renderer::sizef ( const float w, const float h ) const {
  size ( static_cast< std::int32_t > ( w ), static_cast< std::int32_t > ( h ) );
}

#if SDL_VERSION_ATLEAST( 2, 0, 10 )

Rectf_t Renderer::viewportf ( ) const {
  auto [ x, y, w, h ] = sdl2::get_render_viewport ( impl ( ) );
  return { static_cast< float > ( x ), static_cast< float > ( y ), static_cast< float > ( w ),
           static_cast< float > ( h ) };
}

#endif
Recti32_t Renderer::viewport ( ) const {
  auto [ x, y, w, h ] = sdl2::get_render_viewport ( impl ( ) );
  return { x, y, w, h };
}

void Renderer::blendMode ( BlendMode mode ) const { sdl2::set_blend_mode ( impl ( ), translate_blend_mode ( mode ) ); }
BlendMode Renderer::blendMode ( ) const { return translate_sdl2_blend_mode ( sdl2::get_blend_mode ( impl ( ) ) ); }
void Renderer::clip ( Recti32Opt_t srcRect ) const {
  const auto srcPtr = [ &srcRect ] ( ) -> decltype ( &srcRect.value ( ) ) {
    if ( srcRect.has_value ( ) ) {
      return &srcRect.value ( );
    }
    return nullptr;
  }( );

  sdl2::set_render_clip ( impl ( ), srcPtr );
}

template < typename D, typename R >
void draw_texture ( SDL_Renderer* renderer,      //
                    SDL_Texture* texture,        //
                    Recti32Opt_t srcRect,        //
                    std::optional< D > dstRect,  //
                    std::optional< R > rotation ) {
  const auto srcPtr = [ &srcRect ] ( ) -> decltype ( &srcRect.value ( ) ) {
    if ( srcRect.has_value ( ) ) {
      return &srcRect.value ( );
    }
    return nullptr;
  }( );
  const auto dstPtr = [ &dstRect ] ( ) -> decltype ( &dstRect.value ( ) ) {
    if ( dstRect.has_value ( ) ) {
      return &dstRect.value ( );
    }
    return nullptr;
  }( );
  if ( rotation.has_value ( ) ) {
    const auto flip = rotation.value ( ).flip;
    const auto angle = rotation.value ( ).angle;
    const auto angleCenter = [ &rotation ] ( ) -> decltype ( &rotation.value ( ).angleCenter.value ( ) ) {
      if ( rotation.value ( ).angleCenter.has_value ( ) ) {
        return &rotation.value ( ).angleCenter.value ( );
      }
      return nullptr;
    }( );
    sdl2::render_texture ( renderer, texture, flip, angle, angleCenter, srcPtr, dstPtr );
  } else {
    sdl2::render_texture ( renderer, texture, srcPtr, dstPtr );
  }
};

void Renderer::drawToTexture ( cid_t id, std::function< void ( ) >&& func ) const {
  if ( const auto iter = gd::GDApp::texture ( ).impl ( ).textures_.find ( id );
       iter != gd::GDApp::texture ( ).impl ( ).textures_.end ( ) && func ) {
    sdl2::render_to_texture ( std::move ( func ), impl ( ), iter->second.get ( ) );
  }
}

void Renderer::drawToTexture ( const TextureDataOpt_t& texture,  //
                               std::function< void ( ) >&& func ) const {
  if ( texture && texture->texturePtr && func ) {
    sdl2::render_to_texture ( std::move ( func ), impl ( ), texture->texturePtr.get ( ) );
  }
}

void Renderer::setTextureTarget ( const TextureDataOpt_t& texture ) const noexcept {
  auto texturePtr = [ &texture ] ( ) -> SDL_Texture* {
    if ( texture.has_value ( ) ) {
      return texture->texturePtr.get ( );
    }
    return nullptr;
  }( );
  sdl2::set_renderer_target ( impl ( ), texturePtr );
}

#if SDL_VERSION_ATLEAST( 2, 0, 10 )

void Renderer::drawTexture ( cid_t id,
                             const Recti32Opt_t srcRect,  //
                             const RectfOpt_t dstRect,    //
                             const RotationfOpt_t rotation ) const {
  if ( const auto iter = gd::GDApp::texture ( ).impl ( ).textures_.find ( id );
       iter != gd::GDApp::texture ( ).impl ( ).textures_.end ( ) ) {
    draw_texture ( impl ( ), iter->second.get ( ), srcRect, dstRect, rotation );
  }
}
void Renderer::drawTexture ( const TextureDataOpt_t& texture,
                             const Recti32Opt_t srcRect,  //
                             const RectfOpt_t dstRect,    //
                             const RotationfOpt_t rotation ) const {
  if ( texture && texture->texturePtr ) {
    draw_texture ( impl ( ), texture->texturePtr.get ( ), srcRect, dstRect, rotation );
  }
}
void Renderer::drawPoint ( const Pointf_t& p1, const Color& color ) const {
  sdl2::render_point ( impl ( ), p1, color );
}
void Renderer::drawPoints ( const std::vector< Pointf_t >& pts, const Color& color ) const {
  if ( !pts.empty ( ) ) {
    sdl2::render_points ( impl ( ), &pts[ 0 ], pts.size ( ), color );
  }
}
void Renderer::drawLine ( const Pointf_t& p1, const Pointf_t& p2, const Color& color ) const {
  sdl2::render_line ( impl ( ), p1, p2, color );
}
void Renderer::drawLineH ( const float y, const float x1, const float x2, const Color& color ) const {
  drawLine ( { x1, y }, { x2, y }, color );
}
void Renderer::drawLineV ( const float x, const float y1, const float y2, const Color& color ) const {
  drawLine ( { x, y1 }, { x, y2 }, color );
}
void Renderer::drawLines ( const std::vector< Pointf_t >& pts, const Color& color ) const {
  if ( !pts.empty ( ) ) {
    sdl2::render_lines ( impl ( ), &pts[ 0 ], pts.size ( ), color );
  }
}
void Renderer::drawLines ( const Pointf_t* pts, std::size_t size, const Color& color ) const {
  sdl2::render_lines ( impl ( ), pts, size, color );
}
void Renderer::drawRect ( const Rectf_t& rect, const Color& color ) const {  //
  sdl2::render_rect ( impl ( ), rect, color );
}
void Renderer::drawRects ( const std::vector< Rectf_t >& rects, const Color& color ) const {
  if ( !rects.empty ( ) ) {
    sdl2::render_rects ( impl ( ), &rects[ 0 ], rects.size ( ), color );
  }
}
void Renderer::drawRectFill ( const Rectf_t& rect, const Color& color ) const {
  sdl2::render_rect_fill ( impl ( ), rect, color );
}
void Renderer::drawRectsFill ( const std::vector< Rectf_t >& rects, const Color& color ) const {
  if ( !rects.empty ( ) ) {
    sdl2::render_rects_fill ( impl ( ), &rects[ 0 ], rects.size ( ), color );
  }
}
#else

void Renderer::drawTexture ( cid_t id,
                             const Recti32Opt_t srcRect,  //
                             const Recti32Opt_t dstRect,  //
                             const Rotationi32Opt_t rotation ) const {
  if ( const auto iter = gd::GDApp::texture ( ).impl ( ).textures_.find ( id );
       iter != gd::GDApp::texture ( ).impl ( ).textures_.end ( ) ) {
    draw_texture ( impl ( ), iter->second.get ( ), srcRect, dstRect, rotation );
  }
}
void Renderer::drawTexture ( const TextureDataOpt_t& texture,  //
                             const Recti32Opt_t srcRect,       //
                             const Recti32Opt_t dstRect,       //
                             const Rotationi32Opt_t rotation ) const {
  if ( texture && texture->texturePtr ) {
    draw_texture ( impl ( ), texture->texturePtr.get ( ), srcRect, dstRect, rotation );
  }
}
void Renderer::drawPoint ( const Pointi32_t& p1, const Color& color ) const {
  sdl2::render_point ( impl ( ), p1, color );
}
void Renderer::drawPoints ( const std::vector< Pointi32_t >& pts, const Color& color ) const {
  if ( !pts.empty ( ) ) {
    sdl2::render_points ( impl ( ), &pts[ 0 ], pts.size ( ), color );
  }
}
void Renderer::drawLine ( const Pointi32_t& p1, const Pointi32_t& p2, const Color& color ) const {
  sdl2::render_line ( impl ( ), p1, p2, color );
}
void Renderer::drawLineH ( const std::int32_t y, const std::int32_t x1, const std::int32_t x2,
                           const Color& color ) const {
  drawLine ( { x1, y }, { x2, y }, color );
}
void Renderer::drawLineV ( const std::int32_t x, const std::int32_t y1, const std::int32_t y2,
                           const Color& color ) const {
  drawLine ( { x, y1 }, { x, y2 }, color );
}
void Renderer::drawLines ( const std::vector< Pointi32_t >& pts, const Color& color ) const {
  if ( !pts.empty ( ) ) {
    sdl2::render_lines ( impl ( ), &pts[ 0 ], pts.size ( ), color );
  }
}
void Renderer::drawLines ( const Pointi32_t* pts, std::size_t size, const Color& color ) const {
  sdl2::render_lines ( impl ( ), pts, size, color );
}
void Renderer::drawRect ( const Recti32_t& rect, const Color& color ) const {  //
  sdl2::render_rect ( impl ( ), rect, color );
}
void Renderer::drawRects ( const std::vector< Recti32_t >& rects, const Color& color ) const {
  if ( !rects.empty ( ) ) {
    sdl2::render_rects ( impl ( ), &rects[ 0 ], rects.size ( ), color );
  }
}
void Renderer::drawRectFill ( const Recti32_t& rect, const Color& color ) const {
  sdl2::render_rect_fill ( impl ( ), rect, color );
}
void Renderer::drawRectsFill ( const std::vector< Recti32_t >& rects, const Color& color ) const {
  if ( !rects.empty ( ) ) {
    sdl2::render_rects_fill ( impl ( ), &rects[ 0 ], rects.size ( ), color );
  }
}

#endif

void Renderer::drawCircle ( const Circlei32_t& circle, const Color& color ) const {
  sdl2::render_circle ( impl ( ), { circle.x, circle.y }, circle.radius, color );
}

#if USE_SDL2_TTF

void Renderer::drawTTFFont ( const FontTTFDataOpt_t& text ) const {
  int x{ 0 };
  if ( text.has_value ( ) ) {
    auto& ttfFont = gd::GDApp::ttf ( ).impl ( ).getCurrentTTFFont ( );
    auto fontData = &ttfFont.fontData[ 0 ];
    for ( const auto& c : text.value ( ).fontChars ) {
      const auto fontDataGlyphIter = fontData->glyphs.find ( c.ch );
      if ( fontDataGlyphIter != fontData->glyphs.end ( ) ) {
        decltype ( c.dstRect ) srcRect = std::nullopt;
        decltype ( c.dstRect ) dstRect = c.dstRect;  // copy over
        dstRect.value ( ).x = x;
        const auto texturePtr = fontDataGlyphIter->second.textureData->texturePtr.get ( );
        draw_texture ( impl ( ), texturePtr, srcRect, dstRect, c.rotation );
        x += dstRect.value ( ).w;
        x += text.value ( ).margin;
      }
    }
  }
}

#endif
}  // namespace gd
