#include "gd/gd_app.hh"
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <chrono>
using Clock = std::chrono::high_resolution_clock;
using ClockType = std::chrono::nanoseconds;

namespace gd {

bool GDApp::stop_{ false };
GDApp::Settings GDApp::settings_;

System* GDApp::systemPtr_{ nullptr };
FileSystem* GDApp::fileSystemPtr_{ nullptr };
Window* GDApp::windowPtr_{ nullptr };
Renderer* GDApp::rendererPtr_{ nullptr };
Texture* GDApp::texturePtr_{ nullptr };
#if USE_SDL2_TTF
FontTTF* GDApp::ttfPtr_{ nullptr };
#endif
Audio* GDApp::audioPtr_{ nullptr };
Camera* GDApp::cameraPtr_{ nullptr };
Stats* GDApp::statsPtr_{ nullptr };

void GDApp::stop ( ) {
  stop_ = true;
#ifdef __EMSCRIPTEN__
  emscripten_cancel_main_loop ( );
#endif
}
System& GDApp::system ( ) { return *GDApp::systemPtr_; }
FileSystem& GDApp::fs ( ) { return *GDApp::fileSystemPtr_; }
Window& GDApp::window ( ) { return *GDApp::windowPtr_; }
Renderer& GDApp::renderer ( ) { return *GDApp::rendererPtr_; }
Texture& GDApp::texture ( ) { return *GDApp::texturePtr_; }
#if USE_SDL2_TTF
FontTTF& GDApp::ttf ( ) { return *GDApp::ttfPtr_; }
#endif
Audio& GDApp::audio ( ) { return *GDApp::audioPtr_; }
Camera& GDApp::camera ( ) { return *GDApp::cameraPtr_; }
Stats& GDApp::stats ( ) { return *GDApp::statsPtr_; }

GDApp::Settings& GDApp::settings ( ) { return GDApp::settings_; }

void GDApp::Mouse::cursor ( bool enable ) noexcept { sdl2::enable_mouse_cursor ( enable ); }
void GDApp::Mouse::relative ( bool enable ) noexcept { sdl2::enable_mouse_relative ( enable ); }
std::tuple< int, int > GDApp::Mouse::position ( sdl2::MouseStateType type ) noexcept {
  return sdl2::mouse_position ( type );
}

bool GDApp::KB::up ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_UP ); }
bool GDApp::KB::down ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_DOWN ); }
bool GDApp::KB::left ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_LEFT ); }
bool GDApp::KB::right ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_RIGHT ); }
bool GDApp::KB::space ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_SPACE ); }
bool GDApp::KB::w ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_W ); }
bool GDApp::KB::a ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_A ); }
bool GDApp::KB::d ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_D ); }
bool GDApp::KB::s ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_S ); }
bool GDApp::KB::one ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_1 ); }
bool GDApp::KB::two ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_2 ); }
bool GDApp::KB::three ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_3 ); }
bool GDApp::KB::four ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_4 ); }
bool GDApp::KB::five ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_5 ); }
bool GDApp::KB::six ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_6 ); }
bool GDApp::KB::seven ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_7 ); }
bool GDApp::KB::eight ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_8 ); }
bool GDApp::KB::nine ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_9 ); }
bool GDApp::KB::zero ( ) noexcept { return sdl2::keyboard ( SDL_SCANCODE_0 ); }

GDApp::GDApp ( ) {
  systemPtr_ = &system_;
  fileSystemPtr_ = &fileSystem_;
  windowPtr_ = &window_;
  rendererPtr_ = &renderer_;
  texturePtr_ = &texture_;
#if USE_SDL2_TTF
  ttfPtr_ = &ttf_;
#endif
  audioPtr_ = &audio_;
  cameraPtr_ = &camera_;
  statsPtr_ = &stats_;
}

void event_loop ( GDApp* app ) {
  app->t0_ = app->t1_;
  app->t1_ = app->time ( );
  app->frameCycle_++;
  //
  // Input
  //
  SDL_Event event;
  while ( SDL_PollEvent ( &event ) != 0 ) {
    switch ( event.type ) {
      case SDL_WINDOWEVENT:
        app->onWindow ( event.window );
        break;
      case SDL_QUIT:
        app->stop ( );
        break;
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        if ( 0 == event.key.repeat ) {
          app->onKeyboard ( event.key, event.type == SDL_KEYDOWN );
        }
        break;
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
        app->onMouseButton ( event.button, event.type == SDL_MOUSEBUTTONDOWN );
        break;
      case SDL_MOUSEMOTION:
        app->onMouseMotion ( event.motion );
        break;
      case SDL_MOUSEWHEEL:
        app->onMouseWheel ( event.wheel );
        break;
      default:
        break;
    }
  }  // end of while
  app->tFrameTime_ = app->t1_ - app->t0_;
  app->tFpsDuration_ = app->t1_ - app->tFps_;
  if ( app->tFpsDuration_ >= 1.0 ) {
    app->fps_ = app->frameCycle_ / app->tFpsDuration_;
    app->tFps_ = app->t1_;
    app->frameCycle_ = 0;
  }

  app->accumulator_ += ( app->tFrameTime_ > app->spiralOfDeath_ ) ? app->spiralOfDeath_ : app->tFrameTime_;
  //
  // Update
  //
  while ( app->accumulator_ >= app->fixedTickRate_ ) {
    app->accumulator_ -= app->fixedTickRate_;
    app->update ( app->fixedTickRate_ );
    app->camera ( ).update ( app->fixedTickRate_ );
    app->stats ( ).update ( app->fixedTickRate_ );
  }

  auto alpha = app->accumulator_ / app->fixedTickRate_;
  auto updateTime = app->time ( );
  //
  // Render
  //
  app->renderer ( ).clear ( );
  app->render ( alpha );
  app->stats_.render ( alpha );
  auto renderTime = app->time ( );
  app->renderer ( ).swap ( );
  app->stats_.addTime ( { ( static_cast< float > ( updateTime - app->t1_ ) ),
                          ( static_cast< float > ( renderTime - updateTime ) ), app->fps_, app->tFrameTime_ } );
}

void GDApp::run ( [[maybe_unused]] int argc, char* argv[] ) {
  // TODO: Read all configs from env and settings files
  // TODO: Overwrite configs via commandline processor
  system ( ).init ( );
  fs ( ).init ( argv[ 0 ] );
  window ( ).init ( );
  renderer ( ).init ( window ( ) );
#if USE_SDL2_TTF
  ttf ( ).init ( );
#endif
  audio ( ).init ( );
  stats_.init ( );

  {
    if ( settings ( ).printInfo ) {
      sdl2::log::log ( "OS %s\n", system ( ).os ( ) );
      sdl2::log::log ( "Compiler %s\n", system ( ).compiler ( ) );
      sdl2::print_info ( );
      renderer ( ).printInfo ( );
      window ( ).printInfo ( );
    }
  }

  const auto [ dimW, dimH ] = gd::GDApp::window ( ).size ( );
  camera ( ).setDimension ( dimW, dimH );

  init ( );

  t0_ = time ( );  // start
  t1_ = time ( );  // end
#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop_arg ( ( em_arg_callback_func ) event_loop, this, 0, 1 );
#else
  while ( !stop_ ) {
    event_loop ( this );
  }
#endif

  destroy ( );
}

void GDApp::onWindow ( const SDL_WindowEvent& event ) {
  switch ( event.event ) {
    case SDL_WINDOWEVENT_RESIZED:
      break;
    default:
      break;
  }
}

void GDApp::onKeyboard ( [[maybe_unused]] const SDL_KeyboardEvent& event, [[maybe_unused]] const bool isPressed ) {}
void GDApp::onMouseButton ( [[maybe_unused]] const SDL_MouseButtonEvent& event,
                            [[maybe_unused]] const bool isPressed ) {}
void GDApp::onMouseMotion ( [[maybe_unused]] const SDL_MouseMotionEvent& event ) {}
void GDApp::onMouseWheel ( [[maybe_unused]] const SDL_MouseWheelEvent& event ) {}

double GDApp::time ( ) const {
  static const auto period{ static_cast< double > ( ClockType::period::num ) /
                            static_cast< double > ( ClockType::period::den ) };
  const auto now = Clock::now ( );
  const auto nowType = std::chrono::time_point_cast< ClockType > ( now );
  const auto epoch = nowType.time_since_epoch ( );
  const auto value = std::chrono::duration_cast< ClockType > ( epoch );
  const auto dur = value.count ( );
  return static_cast< double > ( dur ) * period;
  // return sdl2::timer(true);
}

}  // namespace gd
