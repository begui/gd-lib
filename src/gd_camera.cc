#include "gd/gd_camera.hh"

#include <algorithm>
#include <cmath>

#include "gd/gd_app.hh"

namespace gd {
constexpr float MAX_ZOOM_IN{4.0f};
constexpr float MAX_ZOOM_OUT{1.0f};

void Camera::update ( [[maybe_unused]] double delta ) {
  /*
  if ( shaking_.dur > 0.0 ) {
    // TODO: Look into perlin noise
    shaking_.x = static_cast< float > ( std::round ( std::sin ( shaking_.dur * M_PI * 4 ) * 8 * shaking_.dur ) );
    shaking_.dur = std::max ( 0.0, shaking_.dur - delta );
  }
  */
}

void Camera::reset ( ) {}
// void Camera::move ( float x, float y, float dur ) {}

// void Camera::shake ( double screenShake ) { shaking_.dur = screenShake; }
// void Camera::getShakeX () const {return getX() + shaking_.x;}
//

float Camera::getX ( ) const { return view_.x; }
float Camera::getY ( ) const { return view_.y; }
float Camera::getXOffset ( ) const { return getX ( ) - static_cast< float > ( getW ( ) ) / ( 2.f * uniformScale_ ); }
float Camera::getYOffset ( ) const { return getY ( ) - static_cast< float > ( getH ( ) ) / ( 2.f * uniformScale_ ); }
float Camera::getScale ( ) const { return uniformScale_; }
double Camera::getRotation ( ) const { return uniformRotation_; }

void Camera::addScale ( float scale ) {
  uniformScale_ += scale;
  // sdl2::set_scale(gd::GDApp::window(), uniformScale_, uniformScale_, false);
}
void Camera::setScale ( float scale ) { uniformScale_ = scale; }

std::int32_t Camera::getW ( ) const { return view_.w; }
std::int32_t Camera::getH ( ) const { return view_.h; }

void Camera::setDimension ( std::int32_t w, std::int32_t h ) {
  view_.w = w;
  view_.h = h;
}
void Camera::setPosition ( float x, float y ) {
  view_.x = x;
  view_.y = y;
}
void Camera::translateX ( float x ) { view_.x += x; }
void Camera::translateY ( float y ) { view_.y += y; }
void Camera::translate ( float x, float y ) {
  translateX ( x );
  translateY ( y );
}

}  // namespace gd
