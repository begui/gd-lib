#include "gd/gd_system.hh"

#include "gd/gd_app.hh"

namespace gd {

void System::init ( ) {
  sdl2::init ( );
#if USE_SDL2_TTF
  sdl2::ttf::init ( );
#endif
#if USE_SDL2_IMAGE
  sdl2::image::init ( );
#endif
#if USE_SDL2_MIXER
  const auto& settings = gd::GDApp::settings ( );
  const auto frequency = settings.audio.frequency == 0 ? MIX_DEFAULT_FREQUENCY : settings.audio.frequency;
  // Has noting to do with mixing channels
  const auto stereoChannels = settings.audio.stereoChannel == 0 ? MIX_DEFAULT_CHANNELS : settings.audio.stereoChannel;
  const auto chunksize = settings.audio.chunksize;
  sdl2::mixer::init( frequency, stereoChannels, chunksize );
#endif
}

const char* System::os ( ) const {
  if constexpr ( os::type == OS::Android ) return "Android";
  if constexpr ( os::type == OS::Bsd ) return "Bsd";
  if constexpr ( os::type == OS::Linux ) return "Linux";
  if constexpr ( os::type == OS::Mac ) return "Mac";
  if constexpr ( os::type == OS::Windows ) return "Windows";
  if constexpr ( os::type == OS::Unknown ) return "Unknown";
}
const char* System::compiler ( ) const {
  if constexpr ( compiler::type == COMPILER::Clang ) return "Clang";
  if constexpr ( compiler::type == COMPILER::GNU ) return "GNU";
  if constexpr ( compiler::type == COMPILER::VCC ) return "VCC";
  if constexpr ( compiler::type == COMPILER::MingGW ) return "MingGw";
  if constexpr ( compiler::type == COMPILER::Turbo ) return "Turbo";
  if constexpr ( compiler::type == COMPILER::Unknown ) return "Unknown";
}
bool System::isCoreEnabled ( ) const {
  return sdl2::isInit ( sdl2::InitType::Everything);
}
bool System::isVideoEnabled ( ) const { return sdl2::isVideoEnabled ( ); }
bool System::isAudioEnabled ( ) const { return sdl2::isAudioEnabled ( ); }
bool System::isEventsEnabled ( ) const { return sdl2::isEventsEnabled ( ); }
bool System::isFontEnabled ( ) const {
#if USE_SDL2_TTF
  return sdl2::isTTFEnabled ( );
#else
  return false;
#endif
}

}  // namespace gd
