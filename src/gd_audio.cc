
#include "gd/gd_audio.hh"

#include <functional>
#include <unordered_map>
#include <vector>

#include "gd/gd_app.hh"
namespace gd {
constexpr auto NOTPLAYING{ -1 };
namespace internal {

std::vector< std::int32_t > channels;
std::int32_t music_playing{ -1 };
auto sound_callback = [] ( std::int32_t channel ) {
  gd::internal::channels[ static_cast< std::size_t > ( channel ) ] = NOTPLAYING;
};
auto music_callback = [] ( ) { music_playing = NOTPLAYING; };

}  // namespace internal

class Audio::Impl final {
  friend class Audio;

 private:
  struct music {
   public:
    music ( ) = default;
    explicit music ( std::unique_ptr< std::uint8_t[] >& data, std::int32_t size );

   public:
#if USE_SDL2_MIXER
    sdl2::MixMusicUniqPtr ptr{ nullptr };
#endif

   private:
    std::unique_ptr< std::uint8_t[] > data;
    std::int32_t size;
  };
  struct sfx {
   public:
    sfx ( ) = default;
    explicit sfx ( std::unique_ptr< std::uint8_t[] >& data, std::int32_t size );

   public:
#if USE_SDL2_MIXER
    sdl2::MixChunkUniqPtr ptr{ nullptr };
#endif
    int channel{ -1 };  // Represents the last channel it was played on.

   private:
    std::unique_ptr< std::uint8_t[] > data;
    std::int32_t size;
  };

 private:
  //  friend std::function< void(int ) > channel_callback ( );
 private:
  std::unordered_map< std::int32_t, music > music_;
  std::unordered_map< std::int32_t, sfx > sfx_;
};
Audio::Audio ( ) : impl_ ( ) {}
Audio::~Audio ( ) {}  // Required for PIMPL
Audio::Impl& Audio::impl ( ) { return *impl_; }
Audio::Impl& Audio::impl ( ) const { return *impl_; }

std::function< void ( int ) > channel_callback ( ) {
  return [] ( [[maybe_unused]] int channel ) {
    // TODO(bj): do something here?
  };
}

Audio::Impl::music::music ( std::unique_ptr< std::uint8_t[] >& d, std::int32_t s ) {
  this->data = std::move ( d );
  this->size = s;
#if USE_SDL2_MIXER
  ptr = sdl2::mixer::load< sdl2::MixMusicUniqPtr > ( this->data.get ( ), this->size );
#endif
}

Audio::Impl::sfx::sfx ( std::unique_ptr< std::uint8_t[] >& d, std::int32_t s ) {
  this->data = std::move ( d );
  this->size = s;
#if USE_SDL2_MIXER
  ptr = sdl2::mixer::load< sdl2::MixChunkUniqPtr > ( this->data.get ( ), this->size );
#endif
}

void Audio::init ( ) {
  const auto& settings = gd::GDApp::settings ( );
  auto channels = settings.audio.audioChannel;
  if ( channels > 0 ) {
    SDL_LogDebug ( SDL_LOG_CATEGORY_AUDIO, "Allocating %d channels\n", channels );
#if USE_SDL2_MIXER
    sdl2::mixer::sound_allocate_channels ( channels );
#endif
  }

#if USE_SDL2_MIXER
  gd::internal::channels.resize ( sdl2::mixer::sound_get_channels ( ) );
#endif
  SDL_LogDebug ( SDL_LOG_CATEGORY_AUDIO, "Allocated %d channels\n",
                 static_cast< int > ( gd::internal::channels.size ( ) ) );

  std::fill ( gd::internal::channels.begin ( ), gd::internal::channels.end ( ), NOTPLAYING );
#if USE_SDL2_MIXER
  sdl2::mixer::sound_callback ( gd::internal::sound_callback );
  sdl2::mixer::music_callback ( gd::internal::music_callback );
#endif
}

bool Audio::load ( const Audio::Type audioType, std::int32_t id, const std::string& file ) {
  bool rtn{ false };

  if ( gd::GDApp::fs ( ).fileExists ( file ) ) {
    //
    if ( Audio::Type::SFX == audioType ) {
      auto iter = impl ( ).sfx_.find ( id );
      if ( iter == impl ( ).sfx_.end ( ) ) {
        auto [ resource, size ] = gd::GDApp::fs ( ).load ( file );
        if ( resource != nullptr && size > 0LL ) {
          auto [ rtnIter, result ] = impl ( ).sfx_.try_emplace ( id, resource, size );
          rtn = result;
        }
      }
    } else {
      auto iter = impl ( ).music_.find ( id );
      if ( iter == impl ( ).music_.end ( ) ) {
        auto [ resource, size ] = gd::GDApp::fs ( ).load ( file );
        if ( resource != nullptr && size > 0LL ) {
          auto [ rtnIter, result ] = impl ( ).music_.try_emplace ( id, resource, size );
          rtn = result;
        }
      }
    }
  }
  return rtn;
}

void Audio::unload ( const Audio::Type /*audioType*/, const std::int32_t /*id*/ ) {
  // TODO: Implement
  // We shouldn't delete if it's being played
}

void Audio::play ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] std::int32_t id,
                   [[maybe_unused]] const std::int32_t msFadeIn, [[maybe_unused]] const bool repeat ) const {
#if USE_SDL2_MIXER
  if ( Audio::Type::SFX == audioType ) {
    if ( impl ( ).sfx_.find ( id ) != impl ( ).sfx_.end ( ) ) {
      auto channel = sdl2::mixer::sound_play ( impl ( ).sfx_[ id ].ptr.get ( ) );
      impl ( ).sfx_[ id ].channel = channel;
      gd::internal::channels[ channel ] = id;
    }
  } else {
    if ( impl ( ).music_.find ( id ) != impl ( ).music_.end ( ) ) {
      // TODO: fix me
      sdl2::mixer::music_play ( impl ( ).music_[ id ].ptr.get ( ), repeat ? -1 : 1, msFadeIn != 0 );
      gd::internal::music_playing = id;
    }
  }

#endif
}  // namespace gd

void Audio::stop ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] std::int32_t id,
                   [[maybe_unused]] const std::int32_t msFade ) const {
#if USE_SDL2_MIXER
  if ( Audio::Type::SFX == audioType ) {
    if ( impl ( ).sfx_.find ( id ) != impl ( ).sfx_.end ( ) ) {
      sdl2::mixer::sound_operation ( sdl2::mixer::AudioOperationType::Stop, impl ( ).sfx_[ id ].channel );
    }
  } else {
    sdl2::mixer::music_halt ( msFade );
  }
#endif
}

void Audio::pause ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] std::int32_t id,
                    [[maybe_unused]] const bool pause ) const {
#if USE_SDL2_MIXER
  if ( Audio::Type::SFX == audioType ) {
    if ( impl ( ).sfx_.find ( id ) != impl ( ).sfx_.end ( ) ) {
      sdl2::mixer::sound_operation (
          pause ? sdl2::mixer::AudioOperationType::Pause : sdl2::mixer::AudioOperationType::Resume,
          impl ( ).sfx_[ id ].channel );
    }
  } else {
    sdl2::mixer::music_operation ( pause ? sdl2::mixer::AudioOperationType::Pause
                                         : sdl2::mixer::AudioOperationType::Resume );
  }
#endif
}

void Audio::volume ( [[maybe_unused]] const Audio::Type audioType, [[maybe_unused]] std::int32_t id,
                     [[maybe_unused]] const std::int32_t vol ) const {
#if USE_SDL2_MIXER
  if ( Audio::Type::SFX == audioType ) {
    if ( impl ( ).sfx_.find ( id ) != impl ( ).sfx_.end ( ) ) {
      sdl2::mixer::sound_volume ( impl ( ).sfx_[ id ].channel, vol );
    }
  } else {
    sdl2::mixer::music_volume ( sdl2::mixer::music_volume ( ) + vol );
  }
#endif
}

void Audio::volumeUp ( const Audio::Type audioType, std::int32_t id, const std::int32_t vol ) const {
  volume ( audioType, id, vol );
}

void Audio::volumeDown ( const Audio::Type audioType, std::int32_t id, const std::int32_t vol ) const {
  volume ( audioType, id, vol );
}

}  // namespace gd
