#include "gd/gd_types.hh"

namespace gd {

Color::Color ( std::uint8_t _r, std::uint8_t _g, std::uint8_t _b, std::uint8_t _a )
    : SDL_Color{ static_cast< Uint8 > ( _r % 256u ), static_cast< Uint8 > ( _g % 256u ),
                 static_cast< Uint8 > ( _b % 256u ), static_cast< Uint8 > ( _a % 256u ) } {}
void Color::red ( std::uint8_t _r ) { r = _r % 256u; }
void Color::green ( std::uint8_t _g ) { g = _g % 256u; }
void Color::blue ( std::uint8_t _b ) { b = _b % 256u; }
void Color::alpha ( std::uint8_t _a ) { a = _a % 256u; }
void Color::rgba ( std::uint8_t _r, std::uint8_t _g, std::uint8_t _b, std::uint8_t _a ) {
  red ( _r );
  green ( _g );
  blue ( _b );
  alpha ( _a );
}
std::uint8_t Color::red ( ) const { return r; }
std::uint8_t Color::green ( ) const { return g; }
std::uint8_t Color::blue ( ) const { return b; }
std::uint8_t Color::alpha ( ) const { return a; }
std::uint32_t Color::toInt ( ) const {
  return static_cast< std::uint32_t > ( a << 24 ) +  //
         static_cast< std::uint32_t > ( b << 16 ) +  //
         static_cast< std::uint32_t > ( g << 8 ) +   //
         static_cast< std::uint32_t > ( r );         //
}
Color::operator std::int32_t ( ) const {
  return ( red ( ) << 24 ) | ( green ( ) << 16 ) | ( blue ( ) << 8 ) | alpha ( );
}
bool Color::operator== ( const Color& other ) const {
  return other.r == r && other.g == g && other.b == b && other.a == a;
}
bool Color::operator!= ( const Color& other ) const {
  return other.r != r || other.g != g || other.b != b || other.a != a;
}

const Color Color::RED{ 0xFF, 0x00, 0x00, 0xFF };
const Color Color::GREEN{ 0x00, 0xFF, 0x00, 0xFF };
const Color Color::BLUE{ 0x00, 0x00, 0xFF, 0xFF };
const Color Color::BLACK{ 0x00, 0x00, 0x00, 0xFF };
const Color Color::WHITE{ 0xFF, 0xFF, 0xFF, 0xFF };
const Color Color::CYAN{ 0x00, 0xFF, 0xFF, 0xFF };
const Color Color::GRAY{ 0x82, 0x82, 0x82, 0xFF };
const Color Color::MAROON{ 0x80, 0x00, 0x00, 0xFF };
const Color Color::OLIVE{ 0x80, 0x80, 0x0, 0xFF };
const Color Color::PURPLE{ 0x80, 0x00, 0x80, 0xFF };
const Color Color::YELLOW{ 0xFF, 0xFF, 0x00, 0xFF };

template < typename T >
constexpr bool Point< T >::operator== ( const Point& other ) const {  //
  return ( this->x == other.x ) && ( this->y == other.y );
}
template < typename T >
constexpr bool Point< T >::operator!= ( const Point& other ) const {  //
  return !this->operator== ( other );
}
template < typename T >
constexpr bool Point< T >::operator< ( const Point& other ) const {  //
  return ( ( this->x < other.x ) || ( ( this->x == other.x ) && ( this->y < other.y ) ) );
}
template < typename T >
void Point< T >::operator*= ( const Point& other ) {
  this->x *= other.x;
  this->y *= other.y;
}
template < typename T >
void Point< T >::operator/= ( const Point& other ) {
  this->x /= other.x;
  this->y /= other.y;
}
template < typename T >
constexpr bool Rect< T >::operator== ( const Rect< T >& other ) const {  //
  return ( this->x == other.x ) && ( this->y == other.y ) && ( this->w == other.w ) && ( this->h == other.h );
}
template < typename T >
constexpr bool Rect< T >::operator!= ( const Rect< T >& other ) const {  //
  return !this->operator== ( other );
}

}  // namespace gd
