#include "gd/gd_filesystem.hh"
#include <physfs.h>
#include <stdexcept>

namespace gd {
auto error_string = []( ) -> std::string {
#if ( PHYSFS_VER_MAJOR > 2 || ( PHYSFS_VER_MAJOR == 2 && PHYSFS_VER_MINOR >= 1 ) )
  auto errorCode = ::PHYSFS_getLastErrorCode ( );
  return ::PHYSFS_getErrorByCode ( errorCode );
#else
  return ::PHYSFS_getLastError ( );
#endif
};

struct PhysFSDeleter {
  void operator( ) ( PHYSFS_file *ptr ) {
    if ( ptr != nullptr ) {
      if ( 0 == ::PHYSFS_close ( ptr ) ) {
        std::runtime_error ( "PhysFS: Failed close handle. " + error_string ( ) );
      }
    }
  }
};

using PhysFSUniqPtr = std::unique_ptr<::PHYSFS_file, PhysFSDeleter >;

inline auto file_open_read ( const std::string &file ) {
  auto ptr = ::PHYSFS_openRead ( file.c_str ( ) );
  if ( ptr == nullptr ) {
    throw std::runtime_error ( "Failed on openRead. " + error_string ( ) );
  }
  return PhysFSUniqPtr{ptr};
}

inline auto file_length ( PhysFSUniqPtr &ptr ) {
  auto rtn = ::PHYSFS_fileLength ( ptr.get ( ) );
  if ( rtn == -1 ) {
    throw std::runtime_error ( "Unable to determine file length." );
  }
  return rtn;
}

void FileSystem::init ( const std::string &basePath ) const {
  if (::PHYSFS_init ( basePath.c_str ( ) ) == 0 ) {
    std::runtime_error ( "PhysFS: Failed to Initialize. " + error_string ( ) );
  }

  if ( const char *gd_asset_path = std::getenv ( "GD_ASSET_PATH" ) ) {
    if ( mount ( gd_asset_path ) == false ) {
      throw std::runtime_error ( "Failed to mount GD_ASSET_PATH" );
    }
  }
}

std::tuple< std::unique_ptr< std::uint8_t[] >, std::size_t > FileSystem::load ( const std::string &file ) const {
  if ( !fileExists ( file ) ) {
    throw std::runtime_error ( "File  " + file + " DNE" );
  }
  auto physfsFile = file_open_read ( file );
  if ( physfsFile != nullptr ) {
    const PHYSFS_sint64 size = file_length ( physfsFile );
    if ( size > 0L ) {
      auto buffer = new std::uint8_t[ size + 1 ];
      if ( size != ::PHYSFS_readBytes ( physfsFile.get ( ), buffer, size ) ) {
        delete[]( buffer );
        throw std::runtime_error ( "File System error while reading file. " + error_string ( ) );
      }
      buffer[ size ] = 0;  // Do we need to set the last index to 0?
      return std::make_tuple ( std::unique_ptr< std::uint8_t[] > ( buffer ), size );
    }
  }
  return std::make_tuple ( std::unique_ptr< std::uint8_t[] > ( nullptr ), 0LL );
}

inline auto stat ( const std::string &fname ) {
  PHYSFS_Stat stat;
  ::PHYSFS_stat ( fname.c_str ( ), &stat );
  return stat;
}

bool FileSystem::fileExists ( const std::string &file ) const {
  const auto rtn = stat ( file );
  return rtn.filetype == PHYSFS_FILETYPE_REGULAR;
}

bool FileSystem::dirExists ( const std::string &dir ) const {
  const auto rtn = stat ( dir );
  return rtn.filetype == PHYSFS_FILETYPE_DIRECTORY;
}

bool FileSystem::mount ( const std::string &archiveOrDir, const char *mountpoint ) const {
  //
  return PHYSFS_mount ( archiveOrDir.c_str ( ), mountpoint, 1 ) != 0;
}
bool FileSystem::unmount ( const std::string &archiveOrDir ) const {
  //
  return PHYSFS_unmount ( archiveOrDir.c_str ( ) ) != 0;
}

}  // namespace gd
