#include "gd/gd_stats.hh"

#include <array>
#include <cmath>
#include <deque>
#include <functional>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>

#include "gd/gd_app.hh"

namespace gd {

template < template < typename, typename > class Container, typename Value,
           typename Allocator = std::allocator< Value >, typename I >
I average_over_container ( const Container< Value, Allocator >& c, I init,
                           const std::function< float ( const gd::Stats::Time& ) >& member ) {
  auto size = c.size ( );
  return size == 0
             ? static_cast< float > ( size )
             : std::accumulate ( c.begin ( ),
                                 c.end ( ),  //
                                 init,       //
                                 [ & ] ( I sum, const gd::Stats::Time& curr ) { return sum + member ( curr ); } ) /
                   static_cast< float > ( size );
};

class Stats::Impl final {
  friend class Stats;

 public:
  Impl ( ) = default;
  Impl ( const Impl& ) = delete;
  Impl& operator= ( const Impl& ) = delete;
  Impl ( Impl&& ) = delete;
  Impl& operator= ( Impl&& ) = delete;
  ~Impl ( ) {}

 private:
  std::deque< Time > frameTimes_;

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  std::array< gd::Pointf_t, Stats::COUNT > updateTimes_;
  std::array< gd::Pointf_t, Stats::COUNT > drawTimes_;
#else
  std::array< gd::Pointi32_t, Stats::COUNT > updateTimes_;
  std::array< gd::Pointi32_t, Stats::COUNT > drawTimes_;

#endif

  float fps_{ 0.f };
  float frameTime_{ 0.f };
  float updateTime_{ 0.f };
  float renderTime_{ 0.f };

  std::optional< TextureData > textureTarget_{ std::nullopt };
#if USE_SDL2_TTF

  std::optional< gd::FontTTFData > lineWinSize;
  std::optional< gd::FontTTFData > line1;
  std::optional< gd::FontTTFData > line2;
  std::optional< gd::FontTTFData > line3;
  std::optional< gd::FontTTFData > line4;

#endif

  bool hide_{ true };
};
Stats::Stats ( ) {}
Stats::~Stats ( ) {}  // Required for PIMPL
Stats::Impl& Stats::impl ( ) { return *impl_; }
Stats::Impl& Stats::impl ( ) const { return *impl_; }

void Stats::init ( ) {
  const auto [ w, h ] = gd::GDApp::renderer ( ).size ( );
  const auto wh = static_cast< std::int32_t > ( w / 2 );
  const auto hh = static_cast< std::int32_t > ( h / 2 );

  impl ( ).textureTarget_ = gd::GDApp::texture ( ).createTarget ( wh, hh );
#if USE_SDL2_TTF
  gd::GDApp::ttf ( ).loadInternal ( gd::data::font::OpenSansRegular, 12 );
#endif
}

void Stats::addTime ( Time&& time ) {
  if ( impl ( ).frameTimes_.size ( ) >= gd::Stats::COUNT ) {
    impl ( ).frameTimes_.pop_front ( );
  }
  impl ( ).frameTimes_.emplace_back ( std::move ( time ) );
}

void Stats::update ( double delta ) {
  if ( impl ( ).hide_ ) {
    return;
  }

  impl ( ).fps_ = average_over_container ( impl ( ).frameTimes_, 0.0f, &Time::fps );
  impl ( ).frameTime_ = average_over_container ( impl ( ).frameTimes_, 0.0f, &Time::frameTime );
  impl ( ).updateTime_ = average_over_container ( impl ( ).frameTimes_, 0.0f, &Time::update );
  impl ( ).renderTime_ = average_over_container ( impl ( ).frameTimes_, 0.0f, &Time::draw );

#if USE_SDL2_TTF
  gd::GDApp::ttf ( ).setIdAndHeight ( gd::data::font::OpenSansRegular, 12 );
  std::int32_t fontHeight = 0;
  auto write_line = [ &fontHeight ] ( auto& fontData, Color color, auto&&... args ) {
    std::stringstream ss;
    // Simple Fold Expression
    // ss << std::fixed;
    ( ( ss << args << " " ), ... );
    gd::GDApp::ttf ( ).writeSolidString ( fontData, ss.str ( ), color );
    for ( auto& ch : fontData->fontChars ) {
      ch.setY ( fontHeight );
    }
    fontHeight += gd::GDApp::ttf ( ).getLineSkip ( );
  };

  // TODO: This is expensive. Look into a way to cache
  const auto [ winW, winH ] = gd::GDApp::window ( ).size ( );
  const auto [ rndW, rndH ] = gd::GDApp::renderer ( ).size ( );

  // write_line ( impl ( ).fontDataStr, Color::RED, "Upd Time:", std::fixed, impl ( ).updateTime_ );

  write_line ( impl ( ).lineWinSize, Color::YELLOW, "WinSize", winW, winH, "RndSize", rndW, rndH );
  write_line ( impl ( ).line1, Color::YELLOW, "Frames Per Second: ", std::fixed, impl ( ).fps_ );
  write_line ( impl ( ).line2, Color::YELLOW, "Frm Time: ", std::fixed, impl ( ).frameTime_ );
  write_line ( impl ( ).line3, Color::RED, "Upd Time: ", std::fixed, impl ( ).updateTime_ );
  write_line ( impl ( ).line4, Color::GREEN, "Drw Time: ", std::fixed, impl ( ).renderTime_ );

#endif

  const auto [ w, h ] = gd::GDApp::renderer ( ).sizef ( );
  const auto wh{ w / 2 };
  const auto hh{ h / 2 };

  const auto graphHeight{ 20 };
  const auto graphStepX = wh / static_cast< float > ( Stats::COUNT - 1 );
  //
  auto transformX = [ graphStepX ] ( auto i ) { return ::floorf ( static_cast< float > ( i ) * graphStepX + 0.5f ); };
  // Should probably use the fixedTickRate_ in GDApp.
  auto transformY = [ hh, graphHeight ] ( auto stime, auto frameTime ) {
    float result = hh - ::floorf ( ( stime / frameTime ) * static_cast< float > ( graphHeight ) + 0.5f );
    return result * 0.95f;
  };

  std::size_t i{ 0 };
  for ( auto&& times : impl ( ).frameTimes_ ) {
    const float tX = transformX ( i );
    const float updateY = transformY ( times.update, static_cast< float > ( delta ) );
    const auto drawY = transformY ( times.draw, static_cast< float > ( delta ) );

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
    impl ( ).updateTimes_[ i ].x = tX;
    impl ( ).updateTimes_[ i ].y = updateY;

    impl ( ).drawTimes_[ i ].x = tX;
    impl ( ).drawTimes_[ i ].y = drawY;
#else
    impl ( ).updateTimes_[ i ].x = static_cast< std::int32_t > ( tX );
    impl ( ).updateTimes_[ i ].y = static_cast< std::int32_t > ( updateY );

    impl ( ).drawTimes_[ i ].x = static_cast< std::int32_t > ( tX );
    impl ( ).drawTimes_[ i ].y = static_cast< std::int32_t > ( drawY );

#endif
    ++i;
  }

#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  const auto whh = wh;
  const auto hhh = hh;
#else
  const auto whh = static_cast< std::int32_t > ( wh );
  const auto hhh = static_cast< std::int32_t > ( hh );
#endif
  // Render To Texture... This is very expensive to do... Should always be called in the update loop
  gd::GDApp::renderer ( ).drawToTexture ( impl ( ).textureTarget_, [ this, whh, hhh ] ( ) {
    gd::GDApp::renderer ( ).drawRectFill ( { 0, 0, whh, hhh }, { 0x82, 0x82, 0x82, 0xC8 } );
    gd::GDApp::renderer ( ).drawRect ( { 0, 0, whh, hhh }, gd::Color::BLUE );
    gd::GDApp::renderer ( ).drawLines ( &impl ( ).updateTimes_[ 0 ], impl ( ).frameTimes_.size ( ), gd::Color::RED );
    gd::GDApp::renderer ( ).drawLines ( &impl ( ).drawTimes_[ 0 ], impl ( ).frameTimes_.size ( ), gd::Color::GREEN );

#if USE_SDL2_TTF
    gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).lineWinSize );
    gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line1 );
    gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line2 );
    gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line3 );
    gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line4 );

#endif
  } );
}

void Stats::render ( [[maybe_unused]] double alpha ) {
  if ( impl ( ).hide_ ) return;

  const auto [ w, h ] = gd::GDApp::renderer ( ).size ( );
#if SDL_VERSION_ATLEAST( 2, 0, 10 )
  const auto wh = static_cast< float > ( w / 2 );
  const auto hh = static_cast< float > ( h / 2 );
#else
  const auto wh = static_cast< std::int32_t > ( w / 2 );
  const auto hh = static_cast< std::int32_t > ( h / 2 );
#endif
  gd::GDApp::renderer ( ).drawTexture ( impl ( ).textureTarget_, std::nullopt, { { wh, 0, wh, hh } } );

  /*
  gd::GDApp::renderer ( ).drawRectFill ( { 0, 0, wh, hh }, { 0x82, 0x82, 0x82, 0xC8 } );
  gd::GDApp::renderer ( ).drawRect ( { 0, 0, wh, hh }, gd::Color::BLUE );
  gd::GDApp::renderer ( ).drawLines ( &impl ( ).updateTimes_[ 0 ], impl ( ).frameTimes_.size ( ), gd::Color::RED );
  gd::GDApp::renderer ( ).drawLines ( &impl ( ).drawTimes_[ 0 ], impl ( ).frameTimes_.size ( ), gd::Color::GREEN );

#if USE_SDL2_TTF

  gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).lineWinSize );
  gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line1 );
  gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line2 );
  gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line3 );
  gd::GDApp::renderer ( ).drawTTFFont ( impl ( ).line4 );

#endif
*/
}

void Stats::reset ( ) { impl ( ).frameTimes_.clear ( ); }
void Stats::toggle ( ) { this->isHidden ( ) ? this->show ( ) : this->hide ( ); }
void Stats::show ( ) { impl ( ).hide_ = false; }
void Stats::hide ( ) { impl ( ).hide_ = true; }

bool Stats::isHidden ( ) const { return impl ( ).hide_; }
}  // namespace gd
