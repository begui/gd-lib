cmake_minimum_required(VERSION 3.9.1)
project(GD VERSION 2021.1.0 DESCRIPTION "Game Dev Library" LANGUAGES CXX)

set(TARGET_NAME ${PROJECT_NAME} )
##########################################################
#--------------------------------------------------------
# The version number.
#--------------------------------------------------------
##########################################################
set(GDLIB_VERSION_MAJOR ${${TARGET_NAME}_VERSION_MAJOR})
set(GDLIB_VERSION_MINOR ${${TARGET_NAME}_VERSION_MINOR})
set(GDLIB_VERSION_PATCH ${${TARGET_NAME}_VERSION_PATCH})
set(GDLIB_VERSION_STRING "${GDLIB_VERSION_MAJOR}.${GDLIB_VERSION_MINOR}.${GDLIB_VERSION_PATCH}")
# configure a header file to pass some of the cmake settings to the source code
configure_file(${PROJECT_SOURCE_DIR}/include/gd/gd_version.hh.in ${PROJECT_BINARY_DIR}/gd/gd_version.hh @ONLY)
#
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
#
add_library(${TARGET_NAME} "")
##########################################################
#--------------------------------------------------------
# Find packages
#--------------------------------------------------------
##########################################################
if( DEFINED EMSCRIPTEN )
  message(STATUS "Building against EMSCRIPTEN version ${EMSCRIPTEN_VERSION}")
  find_program(EMPP_CHECK_COMMAND em++)
  if(NOT EMPP_CHECK_COMMAND)
    message(FATAL_ERROR "Em++ Not found, Make sure to source in the 'source ./emsdk_env.sh' ")
  endif()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s USE_SDL=2 ")
  set(CMAKE_EXECUTABLE_SUFFIX ".html")
else()
  find_package(PkgConfig REQUIRED)
  pkg_search_module(SDL2 REQUIRED sdl2 )
  list(APPEND ${TARGET_NAME}_LIBRARIES ${SDL2_LIBRARIES})
  list(APPEND ${TARGET_NAME}_INCLUDE_DIRS ${SDL2_INCLUDE_DIRS})
  find_package( PhysFS REQUIRED )
  list(APPEND ${TARGET_NAME}_LIBRARIES ${PHYSFS_LIBRARY})
  if(USE_SDL2_IMAGE)
    message(STATUS "Enabling SDL2_image")
    pkg_search_module(SDL2_image REQUIRED SDL2_image)
    list(APPEND ${TARGET_NAME}_LIBRARIES ${SDL2_image_LIBRARIES})
    target_compile_definitions(${TARGET_NAME} INTERFACE USE_SDL2_IMAGE=1)
  endif()
  if(USE_SDL2_TTF)
    message(STATUS "Enabling SDL2_ttf")
    pkg_search_module(SDL2_ttf REQUIRED SDL2_ttf)
    list(APPEND ${TARGET_NAME}_LIBRARIES ${SDL2_ttf_LIBRARIES})
    target_compile_definitions(${TARGET_NAME} INTERFACE USE_SDL2_TTF=1)
  endif()
  if(USE_SDL2_MIXER)
    message(STATUS "Enabling SDL2_mixer")
    pkg_search_module(SDL2_mixer REQUIRED SDL2_mixer)
    list(APPEND ${TARGET_NAME}_LIBRARIES ${SDL2_mixer_LIBRARIES})
    target_compile_definitions(${TARGET_NAME} INTERFACE USE_SDL2_MIXER=1)
  endif()

endif()

##########################################################
#--------------------------------------------------------
# Build the library
#--------------------------------------------------------
##########################################################
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/external/gd-sdl2 EXCLUDE_FROM_ALL)

target_include_directories( ${TARGET_NAME}
  PUBLIC
  $<BUILD_INTERFACE:${${TARGET_NAME}_BINARY_DIR}/include>
  $<BUILD_INTERFACE:${${TARGET_NAME}_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  ${${TARGET_NAME}_INCLUDE_DIRS}
  )

target_link_libraries (${TARGET_NAME}
  PUBLIC
  ${${TARGET_NAME}_LIBRARIES}
  GD_SDL2
  )

target_sources( ${TARGET_NAME}
  PRIVATE
  src/gd_app.cc
  src/gd_audio.cc
  src/gd_camera.cc
  src/gd_data.cc
  src/gd_filesystem.cc
  src/gd_renderer.cc
  src/gd_stats.cc
  src/gd_system.cc
  src/gd_types.cc
  )

target_compile_options(${TARGET_NAME}
  PRIVATE
  $<$<CXX_COMPILER_ID:GNU>:-Wall>
  $<$<CXX_COMPILER_ID:GNU>:-Wextra>
  $<$<CXX_COMPILER_ID:GNU>:-Werror>
  $<$<CXX_COMPILER_ID:GNU>:-Wconversion>
  $<$<CXX_COMPILER_ID:GNU>:-Wshadow>
  )

target_compile_features(${TARGET_NAME}
  PRIVATE
  cxx_std_17
  )
##########################################################
#--------------------------------------------------------
# Build Sandbox
#--------------------------------------------------------
##########################################################
if(${TARGET_NAME}_BUILD_SANDBOX)
  message(STATUS "BUILDING SANDBOX")
  add_subdirectory(sandbox)
endif()

##########################################################
#--------------------------------------------------------
# Build Tests
#--------------------------------------------------------
##########################################################
if(${TARGET_NAME}_BUILD_TEST)
  message(STATUS "BUILDING TEST")
  add_subdirectory(external/Catch2)
  enable_testing()
  add_subdirectory(test)
endif()

