gd-lib
------

UPDATE: This project is officially **DEAD** and will be ARCHIVED. Going to try to do something a bit more minimal using sdl2 in the near future. There will def. be some similarities.

A simple wrapper around SDL2 using modern CC17.

A Slow work in progress.

## References

[SDL2](https://wiki.libsdl.org/CategoryAPI)


## Dependencies

#### Linux

     sudo apt install build-essential cmake automake pkg-config

     sudo apt install  libphysfs-dev

     sudo apt install libsdl2{,-image,-mixer,-ttf}-dev

## Building

You can build with the sandbox project using the following in the **./build** directory

     cmake ../ -DGD_BUILD_SANDBOX=1 -DUSE_SDL2_TTF=1

### Emscripten

You can build via the emscripten toolchain by running the following in the **./build** directory

     cmake ../ -DGD_BUILD_SANDBOX=1 -DCMAKE_TOOLCHAIN_FILE=$EMSCRIPTEN/cmake/Modules/Platform/Emscripten.cmake

