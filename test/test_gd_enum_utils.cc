#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "gd/utils/gd_enum_utils.hh"

enum class Menu {
  SELECTION_BEGIN,  //
  Play,
  Options,
  Quit,
  SELECTION_END
};

TEST_CASE ( "Enum Selection next and prev", "[gd_enum_utils]" ) {
  gd::EnumSelection< Menu > menu;

  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::SELECTION_BEGIN == menu.previous ( ) );

  menu.next ( );
  REQUIRE ( Menu::Options == menu.selected ( ) );
  REQUIRE ( Menu::Play == menu.previous ( ) );

  menu.next ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Options == menu.previous ( ) );

  menu.next ( );
  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::Quit == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Play == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Options == menu.selected ( ) );
  REQUIRE ( Menu::Quit == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::Options == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Play == menu.previous( ) );
}


TEST_CASE ( "Enum Selection next and prev with subset of Menu Items", "[gd_enum_utils]" ) {
  gd::EnumSelection< Menu > menu {Menu::Play, Menu::Quit};

  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::SELECTION_BEGIN == menu.previous ( ) );

  menu.next ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Options == menu.previous ( ) );

  menu.next ( );
  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::Quit == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Play == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Play == menu.selected ( ) );
  REQUIRE ( Menu::Options == menu.previous( ) );

  menu.prev ( );
  REQUIRE ( Menu::Quit == menu.selected ( ) );
  REQUIRE ( Menu::Play == menu.previous( ) );
}

